// JavaScript Document
// ページ共通汎用関数 bianconero, Inc.
// 使用フォント source-han-code-jp-2.000R
// タブ幅は半角スペース8個分

// region // グローバル変数定義 //
var deviceUuid = "";
var storage = localStorage;
var ua = {};	// userAgent 関連
var tmpPath = "";	// iOS App での tmp パス
var moviePath = "";
var imagePath = "";
var SECRET = "f4728acf4b3142a5b5128c743e4c02fd";	// たぶんアプリ共通のシークレットコード。iTunesconnect 発行のやつだと思う。
var licenseJsonObj = null;
var currentMovieId = 0;
var restoreFlag = false;

// 下２行はサウンド再生に使うもの。削除不可。
window.AudioContext = window.AudioContext || window.webkitAudioContext;
context = new AudioContext();

// endregion

// region // ウインドウリサイズ時処理 //
$(window).on('load resize', function(){
	// リサイズ時
	// 画面の縦
		var screenW = $(window).width();
		var screenH = $(window).height();
		//$("#screen").css('height',screenH+'px');
		//log("screenH : " + screenH);
		
		//$("div[id^=page_]").each(function() {
		//	var top = $(this).offset().top;
		//	var divH = Math.ceil(screenH) - Math.ceil(top); // 切り上げ
		//	$(this).height(divH);
		//	//log($(this).prop('id') + ' : ' + top + ' => ' + divH);
		//});
		
	// 表示エリアを考える
		var dispW = 0;
		var dispH = 0;
		var tempDispW = Math.round((parseFloat(screenH) * (4/3)));	// 画面縦幅を基準に考える4:3の横幅
		var tempDispH = Math.round((parseFloat(screenW) * (3/4)));	// 画面横幅を基準に考える4:3の縦幅
	
		// 全域表示できるのはどっちだ？
		if (tempDispW <= screenW) {
			// 左右に黒帯パターン
			dispW = tempDispW;
			dispH = screenH;
		} else {
			// 上下に黒帯パターン
			dispW = screenW;
			dispH = tempDispH;
		}
	
	
	// 基準とするオリジナルのページの縦
		var origPageW = 1280;
		var origPageH = 960;
		
	// 倍率を考える
		var dispScale = dispH / origPageH;
		
	// サイズ変更 screen
		$("div#screen").css("width",dispW+"px");
		$("div#screen").css("height",dispH+"px");
		
	// サイズ変更 screenRelative
		// $("div#screenRelative").css("width",dispW+"px");

	// 倍率変更
		$("div#screenVirtual").css("transform-origin","0 0");
		$("div#screenVirtual").css("transform","scale("+dispScale+")");
	
	// デバッグ出力
		// log(
		//	"onResize : body:" + screenW + "x" +screenH
		//	+ " screen:" + dispW + "x" + dispH
		// );
	
	
	

});
// endregion
// region // 初期設定 //
$(function() {
	onLoad_1 ();
});
function onLoad_1 () {
	
	// 変数初期化
	preTimeFull = -1;
	curTimeFull = 0;
	
	// 機種判断
	
	ua.name = window.navigator.userAgent.toLowerCase();
	ua.isIE = (ua.name.indexOf('msie') >= 0 || ua.name.indexOf('trident') >= 0);
	ua.isiPhone = ua.name.indexOf('iphone') >= 0;
	ua.isiPod = ua.name.indexOf('ipod') >= 0;
	ua.isiPad = ua.name.indexOf('ipad') >= 0;
	ua.isiOS = (ua.isiPhone || ua.isiPod || ua.isiPad);
	ua.isAndroid = ua.name.indexOf('android') >= 0;
	ua.isTablet = (ua.isiPad || (ua.isAndroid && ua.name.indexOf('mobile') < 0));
	ua.isiOSApp = (ua.isiOS || (ua.name.indexOf('safari')<0));
	
	if (ua.isIE) {
		ua.verArray = /(msie|rv:?)\s?([0-9]{1,})([\.0-9]{1,})/.exec(ua.name);
		if (ua.verArray) {
			ua.ver = parseInt(ua.verArray[2], 10);
		}
	}
	if (ua.isiOS) {
		ua.verArray = /(os)\s([0-9]{1,})([\_0-9]{1,})/.exec(ua.name);
		if (ua.verArray) {
			ua.ver = parseInt(ua.verArray[2], 10);
		}
	}
	if (ua.isAndroid) {
		ua.verArray = /(android)\s([0-9]{1,})([\.0-9]{1,})/.exec(ua.name);
		if (ua.verArray) {
			ua.ver = parseInt(ua.verArray[2], 10);
		}
	}
	
	// uuid
	externalGetUUID("onLoad_2");
}
function onLoad_2 (uuid) {
	
	
	deviceUuid = uuid;
	log('onLoad_2 : deviceUuid = "' + deviceUuid + '"');
	
	// tmp のコピー
	externalCopyTmp();
	
	// tmp パスの取得
	externalGetTmpPath("onLoad_3");		// 取得したら onLoad_3 へ飛ぶ。取得内容は引数として渡す。
}
function onLoad_3 (tempTmpPath) {
	if (ua.isiOSApp) {
		tmpPath = "file://" + tempTmpPath;
		moviePath = tmpPath;
		imagePath = tmpPath;
	} else {
		tmpPath = "./tmp/";
		moviePath = "./../media/movie/";
		imagePath = "./../media/image/";
	}
	log('onLoad_3 : tmpPath = "' + tmpPath + '"');
	
	
	// ライセンスの取得
	externalGetLicenses(SECRET);		// onGetLicenses から onLoad_4 へ
}
function onLoad_4 (jsonObj) {
	log('onLoad_4 :');
	licenseJsonObj = jsonObj;
	//log(licenseJsonObj.toString());
	//log(licenseJsonObj[0].toString());
	//log(licenseJsonObj.getKeys());
	//log(true.toString());			// bool は直接出力できないみたいだ。これはいける。
	//log(false.toString());			// bool は直接出力できないみたいだ。これはいける。
	//log(licenseJsonObj[0]["osaka.goodlife.rihasapo.individual.basic.Month"].toString());
	
	
	if (licenseJsonObj["individual1Month"]) {
		// 有効のときの処理はもっと下でやることにする
		
		// log("基本セットライセンス有効");
		// onLoad_5();
	} else {
		log("基本セットライセンス無効");
		// ライセンス無効＆再購入ダイアログの表示
		$("#overlay_background_at99").show();

		if(restoreFlag){
            //リストアしたにも関わらずライセンス無効
            $("#dialog_100expired").show();
		}else{
			//レシートがローカルにないためライセンスを確認できない。
            $("#dialog_99expired").show();
		}

		
		
	}
	
	
	log('individual1Month : ' + licenseJsonObj["individual1Month"].toString());
	// log('movie057 : ' + licenseJsonObj["movie057"].toString());
	// log('movie068 : ' + licenseJsonObj["movie068"].toString());
	// log('movie069 : ' + licenseJsonObj["movie069"].toString());
	// log('movie070 : ' + licenseJsonObj["movie070"].toString());
	// log('movie091 : ' + licenseJsonObj["movie091"].toString());
	// log('movie092 : ' + licenseJsonObj["movie092"].toString());
	// log('movie093 : ' + licenseJsonObj["movie093"].toString());
	// log('movie108 : ' + licenseJsonObj["movie108"].toString());
	// log('movie109 : ' + licenseJsonObj["movie109"].toString());
	// log('movie116 : ' + licenseJsonObj["movie116"].toString());
	// log('movie119 : ' + licenseJsonObj["movie119"].toString());
	// log('movie120 : ' + licenseJsonObj["movie120"].toString());
	// log('movie121 : ' + licenseJsonObj["movie121"].toString());
	// log('movie141 : ' + licenseJsonObj["movie141"].toString());
	// log('movie142 : ' + licenseJsonObj["movie142"].toString());
	// log('movie143 : ' + licenseJsonObj["movie143"].toString());
	
	
	// サウンドを読み込む
	
	// ボタン音
	context.decodeAudioData(data2buffer(SEbutton), function(buffer) {audioBuffer_button = buffer;});
	// 正解音
	context.decodeAudioData(data2buffer(SEcorrect), function(buffer) {audioBuffer_correct = buffer;});
	// 不正解音
	context.decodeAudioData(data2buffer(SEincorrect), function(buffer) {audioBuffer_incorrect = buffer;});
	// リハビリ終了音
	context.decodeAudioData(data2buffer(SEfinish), function(buffer) {audioBuffer_finish = buffer;});
	
	
	// 画面の縦
	var screenH = $(window).height();
	$("#screen").css('height',screenH+'px');
	
	// ボタンアクション
	// iPad で mouseUp が遅いので touchEnd で反応させるためのコード
	// iPad では指を離した後に mouseDown が発火するぞ
	
	if (ua.isiOS) {
		// iOS 専用コード
		$("div[id^=button_]").on({
			"touchstart": function (evt) {buttonMouseDown(this);},
			"touchmove": function (evt) {buttonTouchMove(this,evt);},
			"touchleave": function (evt) {buttonMouseLeave(this);},
			"touchend": function (evt) {buttonMouseUp(this);}
		});
		$("input[id^=radio_]").on({
			"change": function (evt) {radioChange(this);}
		});
		$("select[id^=select_]").on({
			"change": function (evt) {selectChange(this);}
		});
	} else {
		// iOS 以外のコード
		$("div[id^=button_]").on({
			"mousedown": function (evt) {buttonMouseDown(this);},
			"mouseleave": function (evt) {buttonMouseLeave(this);},
			"mouseup": function (evt) {buttonMouseUp(this);}
		});
		$("input[id^=radio_]").on({
			"change": function (evt) {radioChange(this);}
		});
		$("select[id^=select_]").on({
			"change": function (evt) {selectChange(this);}
		});
	}
	
	
	if (licenseJsonObj["individual1Month"]) {
		log("基本セットライセンス有効");
		onLoad_5();
	} else {
		//スクリーンショットを撮るためのコード。普段はコメントに。
		//onLoad_5();
	}
	
}
function onLoad_5 () {
	


	
	// XML 読み込み
	getServerXml();		// この関数内で XML を読み込み、同時に必要ファイルをダウンロードするはず
				// この関数内では UUID を元にサーバーに権利を問い合わせる。

	
	
	// 初期化が終わったのでダイアログを消す
	$("#dialog_99checkFiles").hide();
	$("#overlay_background_at99").hide();
	
	
	
}
// endregion
//region // HTMLから関数をコールさせる処理 //

function buttonMouseDown(elm) {
	// 基本処理
	var id = $(elm).attr("id");
	// console.log("mouseDown : " + id);
	
	// ボタンを青くする
	$(elm).addClass('active');
	
}
function buttonTouchMove(elm,evt) {
	// 基本処理
	var id = $(elm).attr("id");
	// console.log("mouseLeave : " + id);
	
	var touch = evt.touches[0];
	if (elm !== document.elementFromPoint(touch.pageX,touch.pageY)) {
		var event = new CustomEvent("touchleave", { detail: { touches: evt.touches, }, bubbles: false, cancelable: false });
		elm.dispatchEvent(event);
	}
}
function buttonMouseLeave(elm) {
	// 基本処理
	var id = $(elm).attr("id");
	// console.log("mouseLeave : " + id);
	
	// ボタン色を戻す
	$(elm).removeClass('active');
}
function buttonMouseUp(elm) {
	
	// active クラスが付いているか調べる
	// 付いていればこのボタンが押された
	// 付いていなければ領域外で押されているので処理しない
	
	if ($(elm).hasClass("active")) {
		
		// 基本処理
		var id = $(elm).attr("id");
		// console.log("mouseUp : " + id);
		
		// ボタン音を鳴らす
		soundPlay(audioBuffer_button);
		
		// ボタン色を戻す
		$(elm).removeClass('active');
		
		// もし _ で区切って最後の項目を削った関数があるならその関数をキック
		var idArray = id.split("_");
		var functionName = "";
		for (var i = 0; i < idArray.length - 1; i++) { // -1 が最後を削るの意味になる
			functionName += idArray[i] + "_";
		}
		var idLast = idArray[idArray.length - 1];
		if (eval("typeof " + functionName + ' == "function"')) {
			eval(functionName + '("' + id + '","' + idLast + '")');
		}
		
		// もし id と同じ関数名があるならその関数をキック
		if ( eval("typeof " + id + ' == "function"') )  {
			eval(id + '()');
		}
		
	}
	
}

function trMouseDown(elm) {
	// 基本処理
	var id = $(elm).attr("id");
	// log("trMouseDown : mouseDown : " + id);
	
	// 同階層の tr を元の色に戻す
	$(elm).parent().find('tr').removeClass('active');
	
	// tr をオレンジにする
	$(elm).addClass('active');
	
	
	// もし _ で区切って最後の２項目を削った関数があるならその関数をキック
	var idArray = id.split("_");
	var functionName = "";
	for (var i = 0; i < idArray.length - 2; i++) { // -2 が2項目を削るの意味になる
		functionName += idArray[i] + "_";
	}
	if (eval("typeof " + functionName + ' == "function"')) {
		eval(functionName + '("' + id + '")');
	}
	
}

function radioChange(elm) {
	//log("radioChange:" + elm.id);
	
	// 基本処理
	var id = $(elm).attr("id");
	
	// もし _ で区切って最後の項目を削った関数があるならその関数をキック
	var idArray = id.split("_");
	var functionName = "";
	for (var i = 0; i < idArray.length - 1; i++) { // -1 が最後を削るの意味になる
		functionName += idArray[i] + "_";
	}
	var idLast = idArray[idArray.length - 1];
	if (eval("typeof " + functionName + ' == "function"')) {
		eval(functionName + '("' + id + '","' + idLast + '")');
	}
	
	// もし id と同じ関数名があるならその関数をキック
	if ( eval("typeof " + id + ' == "function"') )  {
		eval(id + '()');
	}
}
function selectChange(elm) {
	log("selectChange : " + elm.id);
	
	// 基本処理
	var id = $(elm).attr("id");
	
	// もし _ で区切って最後の項目を削った関数があるならその関数をキック
	var idArray = id.split("_");
	var functionName = "";
	for (var i = 0; i < idArray.length - 1; i++) { // -1 が最後を削るの意味になる
		functionName += idArray[i] + "_";
	}
	var idLast = idArray[idArray.length - 1];
	if (eval("typeof " + functionName + ' == "function"')) {
		eval(functionName + '("' + id + '","' + idLast + '")');
	}
	
	// もし id と同じ関数名があるならその関数をキック
	if ( eval("typeof " + id + ' == "function"') )  {
		eval(id + '("'+ $(elm).val() +'")');
	}
}

// endregion

// region // 定義データの更新 //
function getServerXml () {
	log("getServerXml() が呼ばれた");
	
	$.ajax('https://www.bianconero.co.jp/dev/goodlife/www/api/13a_downloadXml_individual.php',{
			type: 'get'
			, data: {deviceId: deviceUuid}
			, dataType: 'text'	// 一度ローカルストレージに保存するのでわざとtext
			, cache: false
			, crossDomain: true
		})
		.done(function(data) {
			
			
			storage.setItem('settingXml', data);
			//settingXmlというキーの値を削除する場合
			//storage.removeItem('settingXml');
			//ストレージに保存されているデータをすべてクリアする場合
			//storage.clear();
			
			// いる　initializeAll();
			
			
			log("getServerXml : XML:" + data.toString());
			
		})
		.fail(function(data, textStatus, errorThrown) {
			//alert(textStatus); //エラー情報を表示
			log("XML 読み込み失敗");
			//log(errorThrown.message); //例外情報を表示
			log(errorThrown.toString()); //例外情報を表示
			
			// いる　initializeAll();
		})
		.always(function(data, textStatus, returnedObject) {
			// ajaxの通信に成功した場合はdone()と同じ、失敗した場合はfail()と同じ引数を返す
			// alert(textStatus);
			
			downloadAllContents();	// 不足コンテンツダウンロード
			rewriteUserList();	// ユーザーリスト更新 （個人マスター管理）
			rewriteMode();		// モードの変更
			rewriteMenuList1();	// 一般メニューの更新
			rewriteRehabiliMaster();// 個人メニューの選択し更新
			
		});
	
}

function rewriteUserList() {
	
	// log("rewriteUserList : check");
	
	// XML 読み込み
	var xmlStr = storage.getItem('settingXml');
	var xml = $.parseXML(xmlStr);
	
	// 現在選択されているローカルユーザーIDを後の選択用に取り出す
	// var selectedUserLocalId_at92 = $('select_user_at92').val();
	
	// XML上で選択されているローカルユーザーIDを後の選択用に取り出す
	var selectedUserLocalId_at92 = $(xml).children("rihasapo").children("setting").children("userLocalId").text();
	
	// log('rewriteUserList : 元の選択 userLocalId = ' + selectedUserLocalId_at92);
	
	// この後のループで追加される項目を一旦削除
	$('#select_user_at20 option').remove();
	$('#select_user_at20').append('<option value="0">個人を選択してください</option>');
	
	$('#table_userMaster_at91 tbody tr').remove();
	
	$('#select_user_at92 option').remove();
	$('#select_user_at92').append('<option value="0">個人を選択してください</option>');
	
	$('#select_user_at95 option').remove();
	$('#select_user_at95').append('<option value="0">個人を選択してください</option>');
	
	$('#select_user_at98 option').remove();
	$('#select_user_at98').append('<option value="0">個人を選択してください</option>');
	
	
	// <userList> 内のループ
	var cnt = 0;
	var tempUserLocalId = 0;
	
	$(xml).children("rihasapo").children("userList").children("user").each(function(){
		var userServerId	= $(this).attr('userServerId'	);
		var userLocalId		= $(this).attr('userLocalId'	);
		var chartNo		= $(this).attr('chartNo'	);
		var familyName		= $(this).attr('familyName'	);
		var givenName		= $(this).attr('givenName'	);
		
		
		// page_20userMenu の分
		var optionHtml = "";
		optionHtml += "<option";
		optionHtml += ' value="' + userLocalId + '"';
		optionHtml += ">";
		optionHtml += familyName + " " + givenName;
		optionHtml += "</option>";
		// log('rewriteUserList : ' + optionHtml);
		$('#select_user_at20').append(optionHtml);
		
		// page_91userMaster の分
		var trHtml = "";
		trHtml += "<tr id='tr_user_"+userServerId+"_"+userLocalId+"'";
		trHtml +=	" data-chartNo='"+chartNo+"'"
		trHtml +=	" data-familyName='"+familyName+"'"
		trHtml +=	" data-givenName='"+givenName+"'>";
		trHtml += 	"<td>";
		trHtml += 		familyName + " " + givenName;
		trHtml += 	"</td>";
		trHtml += "</tr>";
		$(trHtml).appendTo('#table_userMaster_at91 tbody');
		cnt ++;
		tempUserLocalId = userLocalId;
		
		// page_92mode の分
		$('#select_user_at92').append(optionHtml);
		
		// page_95userMenu の分
		$('#select_user_at95').append(optionHtml);
		
		// page_98userDrill の分
		$('#select_user_at98').append(optionHtml);
	});
	
	// 選択するものは選択しておく
	$('#select_user_at92').val(selectedUserLocalId_at92);
	
	// 普段は選択しないものでもユーザーが1人なら選択していいと思う。
	if (cnt==1) {
		$('#select_user_at20').val(tempUserLocalId);
		$('#select_user_at95').val(tempUserLocalId);
	}
	
	// イベント張り直し
	if (ua.isiOS) {
		// iOS 専用コード
		$("#table_userMaster_at91 tbody tr").on({
			"touchstart": function (evt) {trMouseDown(this);}
			//"touchmove": function (evt) {trTouchMove(this,evt);},
			//"touchleave": function (evt) {trMouseLeave(this);},
			//"touchend": function (evt) {trMouseUp(this);}
		});
	} else {
		// iOS 以外のコード
		$("#table_userMaster_at91 tbody tr").on({
			"mousedown": function (evt) {trMouseDown(this);}
			//"mouseleave": function (evt) {trMouseLeave(this);},
			//"mouseup": function (evt) {trMouseUp(this);}
		});
	}
}
function rewriteMode() {
	// XML 読み込み
	var xmlStr = storage.getItem('settingXml');
	var xml = $.parseXML(xmlStr);
	
	// XML からモードの取得
	var mode = $(xml).children("rihasapo").children("setting").find('mode').text();
	// log('rewriteMode : mode:' + mode);
	
	// ラジオボタンの選択
	if (mode == 'multiUser') {
		$('#radio_mode_multiUser_at92').prop("checked", true);
	} else {
		$('#radio_mode_singleUser_at92').prop("checked", true);
	}
	
	// イベント張り直しは不要
}
function rewriteRehabiliMaster() {
	// XML 読み込み
	var xmlStr = storage.getItem('settingXml');
	var xml = $.parseXML(xmlStr);
	
	// 一般メニューの内容を配列へ（一旦配列に入れないと重複削除が難しい）
	var movArray = [];
	// log(movArray);
	$(xml).children("rihasapo").children("menu").find("movie").each( function() {
		// log('rewriteRehabiliMaster : ' + $(this).attr('name'));
		
		var movieId = parseInt($(this).attr('movieId'),10);
		var mov = {};
		mov['movieId'] = movieId;
		mov['latestVersion']=$(this).attr('latestVersion');
		mov['count']=$(this).attr('count');
		mov['name']=$(this).attr('name');
		
		// 重複ならそのまま、重複してなければ追加
		var findArray = movArray.filter(function(item,index){
			if (item.movieId == movieId) return true;
		});
		if (0 == findArray.length) {
			movArray.push(mov);		// 末尾に追加
		}
		
	});
	
	// ソートしておく（movieId 昇順）
	movArray.sort(function(a,b){
		if(a.movieId<b.movieId) return -1;
		if(a.movieId > b.movieId) return 1;
		return 0;
	});
	
	// 配列デバッグ出力
	// log(JSON.stringify(movArray,null,'\t'));
	
	
	// <option> の作成
	var optHtml = '';
	
	optHtml = '<option></option>';	// 先頭の行だけ追加しておく
	
	movArray.forEach(function(v){
		optHtml += '<option';
		optHtml += ' value="'+ v.movieId +'"';
		optHtml += '>';
		optHtml += v.name;
		optHtml += '</option>';
		optHtml += '\r\n';
	})
	// log(optHtml);
	
	
	// <option> の適用
	$('#select_userMenu01_rehabili option').remove();
	$('#select_userMenu02_rehabili option').remove();
	$('#select_userMenu03_rehabili option').remove();
	$('#select_userMenu04_rehabili option').remove();
	$('#select_userMenu05_rehabili option').remove();
	$('#select_userMenu06_rehabili option').remove();
	$('#select_userMenu07_rehabili option').remove();
	$('#select_userMenu08_rehabili option').remove();
	$('#select_userMenu09_rehabili option').remove();
	$('#select_userMenu10_rehabili option').remove();
	$('#select_userMenu11_rehabili option').remove();
	$('#select_userMenu12_rehabili option').remove();
	
	$('#select_userMenu01_rehabili').append(optHtml);
	$('#select_userMenu02_rehabili').append(optHtml);
	$('#select_userMenu03_rehabili').append(optHtml);
	$('#select_userMenu04_rehabili').append(optHtml);
	$('#select_userMenu05_rehabili').append(optHtml);
	$('#select_userMenu06_rehabili').append(optHtml);
	$('#select_userMenu07_rehabili').append(optHtml);
	$('#select_userMenu08_rehabili').append(optHtml);
	$('#select_userMenu09_rehabili').append(optHtml);
	$('#select_userMenu10_rehabili').append(optHtml);
	$('#select_userMenu11_rehabili').append(optHtml);
	$('#select_userMenu12_rehabili').append(optHtml);
	
	
	
	
}
function downloadAllContents() {
	// XML を読み込み終わるとここが引数なしで呼ばれる。
	// 「とにかく全部読み込む！」っていう関数
	
	if (ua.isiOSApp) {
		externalGetTmpFileList('downloadAllContents_next');
		// とりあえず現在 Tmp フォルダにあるファイルを取ってくる。
		// 取得したら downloadAllContents_next が呼ばれる
	}
}
function downloadAllContents_next(tmpFileList) {
	// とりあえず tmp フォルダにあるファイルが配列になって渡ってくる。
	// ここでする事は、XML にあって、tmp にないファイルをリストアップすること。
	
	log('downloadAllContents_next : '+ tmpFileList);
	
	var tmpArray = [];
	eval('tmpArray='+tmpFileList);	// swift からの返り値を強引に配列変数化
	// log('tmpArray : ' + tmpArray);			// 動作OK
	// log('tmpArray.length : ' + tmpArray.length);		// 動作OK
	
	if (ua.isiOSApp) {
		
		//mskg log('!!!!!!!!!!!!!!!!!Kuroishi:以下 tmpFileList');
		//mskg log(tmpFileList);
	
		// XML 読み込み
		var xmlStr = storage.getItem('settingXml');
		log("まずは解析前の XML -----------------------------------------------");
		log(xmlStr);
		var xml = $.parseXML(xmlStr);
		
		// 必要な movie のピックアップ
		var movArray = [];	// movie の必要情報を配列へ（一旦配列に入れないと重複削除が難しい）
		
		$(xml).find('movie').each(function () {
			var movieId		= parseInt($(this).attr('movieId'), 10);
			var latestVersion	= parseInt($(this).attr('latestVersion'), 10);
			var movieIdXXX		= right('000' + movieId, 3);
			var movieIdXXXX		= right('0000' + movieId, 4);
			var latestVersionXX	= right('00' + latestVersion, 2);
			var movieFilenameA	= movieIdXXX + 'a-' + latestVersionXX + '.mp4';
			var movieFilenameB	= movieIdXXX + 'b-' + latestVersionXX + '.mp4';
			if (movArray.indexOf(movieFilenameA) < 0) {	// 重複防止
				if (movieId != 0) {	// たまに0があるので削除（最終的にはなくなるかも）
					movArray.push(movieFilenameA);
					movArray.push(movieFilenameB);
				}
			}
		});
		log("ダウンロードが必要なムービー -----------------------------------------------");
		log(movArray.toString());
		
		
		
		
		// tmp にないものをピックアップ
		
		var notExist = 0; // テスト用に全部再ダウンロードするならこの値を99999に。本来は０。
		
		var downloadMovieArray = [];
		movArray.forEach(function(val,ix,ar){
				//mskg log('val:'+val+' ix:'+ix);
				if(tmpFileList.indexOf(val) < notExist) {
					downloadMovieArray.push(val);
				}
		});
		
		
		
		//mskg log('!!!!!!!!!!!!!!!!!Kuroishi:downloadMovieArray = ' + downloadMovieArray);
		//mskg log('downloadMovieArray.length : ' + downloadMovieArray.length);
		
		if (0 < downloadMovieArray.length) {
			// 1つ以上のダウンロードファイルがある
			$("#dialog_99downloadFiles").show();
			externalDownloadFilesToTmp(downloadMovieArray);
		} else {
			//  ダウンロード不必要
		}
		
		
		
		
		
		
		// movie のダウンロード
		// 必要な image のピックアップ
		// image のダウンロード
		
	}
}
function downloadAllContents_end() {
	$("#dialog_99downloadFiles").hide();	// ダイアログ消去
	$("＃overlay_background_at99").hide();	// 背景消去
    externalCancelDownloadTasks(); //ネイティブにダウンロードタスクをキャンセルさせる
	
}
function downloadNeedContents() {

}
function downloadContentsCancelEnable() {

}
function downloadContentsCancelDisable() {

}

// endregion


///////////////////////////////////////// 特殊処理ボタン /////////////////////////////////////////////////

// region //-- ダイアログ　----------------------------------------------------------------------------------------
function button_expired_cancel() {
	// アプリを終了する命令がなかったのでここは呼ばれないようにした。
	// ライセンス期限切れダイアログ内のキャンセルボタンにここを呼ぶボタンがある。
}
function button_expired_purchase () {
	
	log("button_expired_purchase");
	
	
	// ライセンス無効＆再購入ダイアログの非表示
	$("#dialog_99expired").hide();
	
	var userId = deviceUuid;
	var prodids = ["individual1Month"];
	externalBuyProduct(userId, prodids);
}
function button_expired_purchase2 () {

    log("button_expired_purchase2");


    // ライセンス無効＆再購入ダイアログの非表示
    $("#dialog_100expired").hide();

    var userId = deviceUuid;
    var prodids = ["individual1Month"];
    externalBuyProduct(userId, prodids);
}

function button_expired_restore () {

    log("button_expired_restore");

    // ライセンス無効＆再購入ダイアログの非表示
    $("#dialog_99expired").hide();

    var userId = deviceUuid;
    // var prodids = ["individual1Month"];
    // externalBuyProduct(userId, prodids);
    externalRestoreProducts(userId);
}
function button_purchaseError_purchase () {
	
	// 購入エラーダイアログの非表示
	$("#dialog_99purchaseError").hide();
	
	// ライセンス無効＆再購入ダイアログの非表示
	$("#dialog_99expired").hide();
	
	var userId = deviceUuid;
	var prodids = ["individual1Month"];
	externalBuyProduct(userId, prodids);
}
// endregion
// region //-- トップページ ---------------------------------------------------------------------------------------

function button_download_cancel() {
	downloadAllContents_end();
}

function button_exitApp_() {
	log("burron_exitApp_");
}

// endregion
// region //-- リハサポ個人メニュー ---------------------------------------------------------------------------------

function button_prevPage_at20() {
	
	// 現在のページを取得
	var currentPage = parseInt(left($('#label_page_at20').text(),1),10);
	
	// 次に表示するページを決定
	currentPage--;
	if (currentPage < 1) {currentPage = 1;}
	
	// ページ表示を更新
	redrawPage_20userMenu(currentPage);
}
function button_nextPage_at20() {
	
	// 現在のページを取得
	var currentPage = parseInt(left($('#label_page_at20').text(),1),10);
	
	// 次に表示するページを決定
	currentPage++;
	if (3 < currentPage) {currentPage = 3;}
	
	// ページ表示を更新
	redrawPage_20userMenu(currentPage);
}
function redrawPage_20userMenu(page) {	// ページ番号に合わせて再描画
	
	// XML 読み込み
	var xmlStr = storage.getItem('settingXml');
	var xml = $.parseXML(xmlStr);
	var privateMenuXml = $(xml).find('rihasapo').children('privateMenu[userLocalId='+$('#select_user_at20').val()+']');
	log('redrawPage_20userMenu : privateMenuXml = '+privateMenuXml.prop("outerHTML"));
	
	// 再描画ループ
	for (var i=1;i<=12;i++) {
		var ii = right('00'+i,2);	// 01〜12
		
		$('#button_privateMovieList'+ii+'_at20').text('');
		
		// リハビリ名の選択
		var movieXml = $(privateMenuXml).find('movie[page='+page+'][slot='+i+']');
		log(movieXml);
		log('movieId = ' + $(movieXml).attr('movieId'));
		
		$('#button_privateMovieList'+ii+'_at20').text($(movieXml).attr('name'));
		// $('#button_privateMovieList'+ii+'_at20').text($(movieXml).attr('movieId'));
		$('#button_privateMovieList'+ii+'_at20').attr('data-movieId',$(movieXml).attr('movieId'));
		
		// 回数の選択
		$('#select_userMenu'+ii+'_count').val($(movieXml).attr('count'));
		
		// 結果の出力
		$('#text_userMenu'+ii+'_result').val($(movieXml).attr('result'));
	}
	
	// ページ表示を更新
	$('#label_page_at20').text(page+'/3');
	
	
}
function select_user_at20 () {
	// log("check");
	redrawPage_20userMenu(1);
}
function button_privateMovieList01_ (id,idLast) {button_privateMovieList(id, 1)}	// ボタン名から引数拡張し主処理へ
function button_privateMovieList02_ (id,idLast) {button_privateMovieList(id, 2)}	// ボタン名から引数拡張し主処理へ
function button_privateMovieList03_ (id,idLast) {button_privateMovieList(id, 3)}	// ボタン名から引数拡張し主処理へ
function button_privateMovieList04_ (id,idLast) {button_privateMovieList(id, 4)}	// ボタン名から引数拡張し主処理へ
function button_privateMovieList05_ (id,idLast) {button_privateMovieList(id, 5)}	// ボタン名から引数拡張し主処理へ
function button_privateMovieList06_ (id,idLast) {button_privateMovieList(id, 6)}	// ボタン名から引数拡張し主処理へ
function button_privateMovieList07_ (id,idLast) {button_privateMovieList(id, 7)}	// ボタン名から引数拡張し主処理へ
function button_privateMovieList08_ (id,idLast) {button_privateMovieList(id, 8)}	// ボタン名から引数拡張し主処理へ
function button_privateMovieList09_ (id,idLast) {button_privateMovieList(id, 9)}	// ボタン名から引数拡張し主処理へ
function button_privateMovieList10_ (id,idLast) {button_privateMovieList(id,10)}	// ボタン名から引数拡張し主処理へ
function button_privateMovieList11_ (id,idLast) {button_privateMovieList(id,11)}	// ボタン名から引数拡張し主処理へ
function button_privateMovieList12_ (id,idLast) {button_privateMovieList(id,12)}	// ボタン名から引数拡張し主処理へ
function button_privateMovieList(id,num) {
	console.log("button_privateMovieList : " + id);
	
	// name 抽出
	var name = $("#"+id).text();
	// var textArray = text.split("_");
	// var name = textArray[textArray.length - 1];
	
	console.log(num + " : " + name );
	

		
		// ムービーからの戻り先として現在のページを覚えておく
		backTo = "#pageRadio_20userMenu";
	
	
		//var movieId = $("#"+id).find('div').attr('data-movieId');
		var movieId = $("#"+id).attr('data-movieId');
		console.log("movieId : " + movieId);
		rewriteMovie(movieId);
		$("#pageRadio_40movie").prop("checked", true);	// ページを移動

}

// endregion
// region //-- リハサポ一般メニュー ---------------------------------------------------------------------------------
function button_movieList01_ (id,idLast) {button_movieList(id,1,idLast)}	// ボタン名から引数拡張し主処理へ
function button_movieList02_ (id,idLast) {button_movieList(id,2,idLast)}	// ボタン名から引数拡張し主処理へ
function button_movieList03_ (id,idLast) {button_movieList(id,3,idLast)}	// ボタン名から引数拡張し主処理へ
function button_movieList04_ (id,idLast) {button_movieList(id,4,idLast)}	// ボタン名から引数拡張し主処理へ
function button_movieList05_ (id,idLast) {button_movieList(id,5,idLast)}	// ボタン名から引数拡張し主処理へ
function button_movieList06_ (id,idLast) {button_movieList(id,6,idLast)}	// ボタン名から引数拡張し主処理へ
function button_movieList07_ (id,idLast) {button_movieList(id,7,idLast)}	// ボタン名から引数拡張し主処理へ
function button_movieList08_ (id,idLast) {button_movieList(id,8,idLast)}	// ボタン名から引数拡張し主処理へ
function button_movieList09_ (id,idLast) {button_movieList(id,9,idLast)}	// ボタン名から引数拡張し主処理へ
function button_movieList10_ (id,idLast) {button_movieList(id,10,idLast)}	// ボタン名から引数拡張し主処理へ
function button_movieList(id,num,idLast) {
	log("button_movieList : " + id +","+num +","+idLast);
	
	// id 末尾抽出
	//var idArray = id.split("_");
	//var idLast = idArray[idArray.length - 1];
	
	// name 抽出
	var name = $("#"+id).text();
	// var textArray = text.split("_");
	// var name = textArray[textArray.length - 1];
	
	// dir か movie かを判断
	var tag = 'none';
	var html = $("#"+id).html();
	if (html.match(/propDir/)) { tag = 'dir';}
	if (html.match(/propMovie/)) { tag = 'movie';}
	
	
	console.log(num + " : " + idLast + " : " + name + " : " + tag);
	
	if (tag == 'movie') {
		// movie の場合
		
		// ムービーからの戻り先として現在のページを覚えておく
		if ($("#pageRadio_30movieList1").prop("checked")) {backTo = "#pageRadio_30movieList1"}
		if ($("#pageRadio_31movieList2").prop("checked")) {backTo = "#pageRadio_31movieList2"}
		if ($("#pageRadio_32movieList3").prop("checked")) {backTo = "#pageRadio_32movieList3"}
		
		console.log("html : " + html);
		var movieId = $("#"+id).find('div').attr('data-movieId');
		console.log("movieId : " + movieId);
		rewriteMovie(movieId);
		$("#pageRadio_40movie").prop("checked", true);	// ページを移動
	} else if (tag == 'dir') {
		// dir の場合
		
		// 末尾による挙動変更
		if (idLast == "at30") {
			name1 = name;
			rewriteMenuList2(name1);
			$("#pageRadio_31movieList2").prop("checked", true);	// ページを移動
		} else if (idLast == "at31") {
			name2 = name;
			rewriteMenuList3(name1, name2);
			$("#pageRadio_32movieList3").prop("checked", true);	// ページを移動
		} else if (idLast == "at32") {
			//rewriteMenuList3();
			$("#pageRadio_40movie").prop("checked", true);	// ページを移動
		}
	} else {
		// none の場合
	}
}				// 下階層への移動 or ムービー再生

function rewriteMenuList1() {
	
	log("rewriteMenuList1 が呼ばれた");
	
	// XML 読み込み
	var xmlStr = storage.getItem('settingXml');
	
	// log("localstorageXML:" + xmlStr);
	
	var xml = $.parseXML(xmlStr);
	
	var count = 0;
	
	// タイトル更新
	$("#title_movieList1").text("リハサポメニュー");
	
	// メニュー更新
	$(xml).children("rihasapo").children("menu").children("dir,movie").each(function(){
		var name = $(this).attr('name');
		var tag = $(this).prop("tagName");
		var movieId = $(this).attr("movieId");
		log("rewriteMenuList1 : " + tag + " : " + name + " : " + $(this).attr("movieId"));
		
		count++ ;
		id = "button_movieList" + right("00"+count,2) + "_at30";
		if (tag=="dir") {
			$("#"+id).html("<div class='propDir'></div>" + name);
		} else {
			$("#"+id).html("<div class='propMovie1' data-movieId='"+movieId+"'></div>" + name);
		}
	})
	
	// 残りメニューを初期化
	while (count<12) {
		count++ ;
		id = "button_movieList" + right("00"+count,2) + "_at30";
		$("#"+id).text("");
		$("#"+id).css('display', 'none');
	}
}
function rewriteMenuList2(dirName) {
	// XML 読み込み
	var xmlStr = storage.getItem('settingXml');
	var xml = $.parseXML(xmlStr);
	
	var count = 0;
	
	// タイトル更新
	$("#title_movieList2").text(dirName);
	
	// メニュー更新
	$(xml).children("rihasapo").children("menu").children("dir[name='"+dirName+"']").children("dir,movie").each(function(){
		var name = $(this).attr('name');
		var tag = $(this).prop("tagName");
		var movieId = $(this).attr("movieId");
		log("rewriteMenuList2 : " +tag + " : " + name + " : " + $(this).attr("movieId"));
		
		count++ ;
		id = "button_movieList" + right("00"+count,2) + "_at31";
		if (tag=="dir") {
			$("#"+id).html("<div class='propDir'></div>" + name);
		} else {
			$("#"+id).html("<div class='propMovie1' data-movieId='"+movieId+"'></div>" + name);
		}
		$("#"+id).css('display', 'block')
	})
	
	// 残りメニューを初期化
	while (count<12) {
		count++ ;
		id = "button_movieList" + right("00"+count,2) + "_at31";
		$("#"+id).text("");
		$("#"+id).css('display', 'none');
	}
}
function rewriteMenuList3(dirName1,dirName2) {
	// XML 読み込み
	var xmlStr = storage.getItem('settingXml');
	var xml = $.parseXML(xmlStr);
	
	var count = 0;
	
	// タイトル更新
	$("#title_movieList3").text(dirName2);
	
	// メニュー更新
	$(xml).children("rihasapo").children("menu").children("dir[name='"+dirName1+"']").children("dir[name='"+dirName2+"']").children("dir,movie").each(function(){
		var name = $(this).attr('name');
		var tag = $(this).prop("tagName");
		var movieId = $(this).attr("movieId");
		var charge = $(this).attr("status");
		log("rewriteMenuList3 : " +tag + " : " + name + " : " + $(this).attr("movieId"));
		
		
		count++ ;
		id = "button_movieList" + right("00"+count,2) + "_at32";
		if (tag=="dir") {
			$("#"+id).html("<div class='propDir'></div>" + name);
		} else {
			$("#"+id).html("<div class='propMovie' data-movieId='"+movieId+"'></div>" + name);
		}
		$("#"+id).css('display', 'block');
		
		
		id = "preview_movieList" + right("00"+count,2) + "_at32";
		if (charge=="使用可") {
			$("#" + id).css('display', 'none');
		} else if (charge=="追加有料") {
			$("#" + id).css('display', 'block');
		}
	})
	
	// 残りメニューを初期化
	while (count<12) {
		count++ ;
		id = "button_movieList" + right("00"+count,2) + "_at32";
		$("#"+id).text("");
		$("#"+id).css('display', 'none');
		id = "preview_movieList" + right("00"+count,2) + "_at32";
		$("#"+id).css('display', 'none');
	}
}
function rewriteMovie(movieIdNum) {
	
	currentMovieId = movieIdNum;
	var movieId = right('000' + movieIdNum,3);
	
	// XML 読み込み
	var xmlStr = storage.getItem('settingXml');
	var xml = $.parseXML(xmlStr);
	
	// movieId を元に XML から情報を取得
	var preMovieFile = moviePath + movieId+"a-01.mp4";
	log("rewriteMovie : " + preMovieFile);
	var movieFile = moviePath + movieId+"b-01.mp4";
	var movieCount = $(xml).children("rihasapo").children("menu").find("movie[movieId='"+movieId+"']").first().attr('count');
	var charge = $(xml).children("rihasapo").children("menu").find("movie[movieId='"+movieId+"']").first().attr('status');
	
	$("#videoA"			).attr('src',preMovieFile);
	$("#videoB"			).attr('src',movieFile);
	$("#videoA"			).css('display', 'block');		// 解説ムービーを表示
	$("#videoB"			).css('display', 'none');		// 本編ムービーは非表示
	$("#button_exitMovie_1"		).css('display', 'block');
	$("#title_movie"		).css('display', 'block');
	if (charge=="追加有料") {
		
		$("div[id^=title_movie]").css('display', 'none');		// 「再生する回数を選んで下さい」 非表示
		$("div[id^=title_purchase]").css('display', 'block');		// 「課金動画です購入して下さい」 表示
		
		$("div#button_purchase120").css('display', 'block');		// 「購入ボタン」 表示
		
		$("div[id^=button_play01z05_]").css('display', 'none');		// 01〜05 非表示
		$("div[id^=button_play10z50_]").css('display', 'none');		// 01〜05 非表示
		
	} else {
		
		$("div[id^=title_movie]").css('display', 'block');		// 「再生する回数を選んで下さい」 表示
		$("div[id^=title_purchase]").css('display', 'none');		// 「課金動画です購入して下さい」 非表示
		
		$("div#button_purchase120").css('display', 'none');		// 「購入ボタン」 非表示
		
		if (movieCount == "01-05") {						// XMLの movieCount が"01-05"
			$("div[id^=button_play01z05_]").css('display', 'block');	// 01〜05 表示
			$("div[id^=button_play10z50_]").css('display', 'none');		// 10〜50 非表示
		} else if (movieCount == "10-50") {					// XMLの movieCount が"10-50"
			$("div[id^=button_play01z05_]").css('display', 'none');		// 01〜05 非表示
			$("div[id^=button_play10z50_]").css('display', 'block');	// 01〜05 表示
		} else {								// XMLの movieCount が想定外
			$("div[id^=button_play01z05_]").css('display', 'none');		// 01〜05 非表示
			$("div[id^=button_play10z50_]").css('display', 'none');		// 01〜05 非表示
		}
	}
	
	$("#overlay_totalPlayTimes"	).css('display', 'none');
	$("#overlay_remainingPlayTimes"	).css('display', 'none');
	$("#overlay_counter"		).css('display', 'none');
	
	// 巻き戻し
	document.getElementById("videoB").currentTime = 0;
	preTimeFull = -1;
	curTimeFull = 0;
}

function button_purchase120 () {
	log("動画を購入する：" + currentMovieId);
	
	var movieIdXXX = right('000' + currentMovieId,3);
	
	var userId = deviceUuid;
	var prodids = ["movie" + movieIdXXX];
	externalBuyProduct(userId, prodids);
	
}

// endregion
// region //-- リハサポ動画再生部分 ---------------------------------------------------------------------------------
function button_exitMovie_ () {
	$(backTo).prop("checked", true);	// ページを移動
	document.getElementById('videoA').pause();
	document.getElementById('videoB').pause();
	clearInterval(timer);
	preTimeFull = -1;
}			// ムービー画面からの「戻る」。飛び先可変。

function button_play01z05_ (id,idLast) {
	// console.log("button_play01z05(" + id + "," + idLast + ")");
}		//
function button_play10z50_ (id,idLast) {
	// console.log("button_play10z50(" + id + "," + idLast + ")");
	
	// ボタンの消去
	$("div#title_movie").css('display','none');			// 再生する回数を選んで下さい　非表示
	$("div[id^=button_play10z50_]").css('display', 'none');		// 10〜50 非表示
	
	// 残カウンターの初期化
	remainingPlayTimes = parseInt(idLast,10);			// 残カウンター変数に代入
	
	// オーバーレイの表示
	$("div#overlay_totalPlayTimes").text(idLast);			// 回数を書き換え
	$("div#overlay_totalPlayTimes").css('display','block');		// 回数を表示
	$("span#span_remainingPlayTimes").text(remainingPlayTimes);	// 残カウンター書き換え
	$("div#overlay_remainingPlayTimes").css('display','block');	// 残カウンターを表示
	$("div#overlay_counter").css('display','block');		// 秒カウンタ表示
	
	// setInterval の設定
	timer = setInterval("counterUpdate()",33);			// 関数を33ミリ秒間隔で呼び出す(30fps)
	
	// ムービーの切り替え
	$("#videoB").css('display', 'block');				// 背面の本編を表示
	document.getElementById("videoB").play();			// 背面の本編を再生
	$("#videoA").css('display', 'none');				// 前面の解説を非表示
	document.getElementById("videoA").pause();			// 前面の解説を停止
	
}		//
function counterUpdate() {
	v = document.getElementById("videoB");
	preTimeFull = curTimeFull;
	curTimeFull = v.currentTime;
	//curTimeInt = Math.ceil(curTimeFull); // 切り上げ
	//curTimeInt = Math.floor(curTimeFull); // 切り捨て
	curTimeInt = Math.round(curTimeFull); // 四捨五入
	
	// 表示するカウント数を考える
	displayCount = "0";	// 0 か空白か少し悩む
	if (v.duration < 15) {	// １５秒未満なら１〜８のカウントは１回だろう
		if ((1 <= curTimeInt) && (curTimeInt <= 8)) {
			displayCount = curTimeInt;
		}
	} else {		// １５秒以上あるので１〜８のカウントを２回行う 例．マシーン腹筋背筋
		if ((1 <= curTimeInt) && (curTimeInt <= 8)) {
			displayCount = curTimeInt;	// 1〜8 ならそのまま表示
		} else if ((9 <= curTimeInt) && (curTimeInt <= 16)) {
			displayCount = curTimeInt-8;	// 9〜16 なら 1〜8 に変換表示
		}
	}
	
	// カウント数によって色を変えて表示する
	if ((0 <= displayCount) && (displayCount <= 4)) {
		$("div#overlay_counter").css("color","#F00");	// 0〜4 なら赤
	} else if ((5 <= displayCount) && (displayCount <= 8)) {
		$("div#overlay_counter").css("color","#00F");	// 5〜8 なら青
	}
	$("div#overlay_counter").text(displayCount);
	
	// もしループで巻き戻しが起こったら
	if (curTimeFull<preTimeFull) {
		console.log("cur:"+curTimeFull+" < pre:"+preTimeFull);
		remainingPlayTimes--;
		if (0 < remainingPlayTimes) {
			$("span#span_remainingPlayTimes").text(remainingPlayTimes);
		} else {
			// 完了
			$("#pageRadio_41movieEnd").prop("checked", true);	// ページを移動
			soundPlay(audioBuffer_finish);
			document.getElementById('videoA').pause();
			document.getElementById('videoB').pause();
			clearInterval(timer);
			preTimeFull = -1;
		}
	}
}				//
// endregion
// region //-- 認知症予防ドリル ------------------------------------------------------------------------------------
function button_answer1_ () {
	$("#pageRadio_52correct"	).prop("checked", true);	// ページを移動
	// 正解音を鳴らす
	soundPlay(audioBuffer_correct);
}			// 暫定。本来はムービーのように引数拡張して主処理へ
function button_answer2_ () {
	$("#pageRadio_53wrong"		).prop("checked", true);	// ページを移動
	// 不正解音を鳴らす
	soundPlay(audioBuffer_incorrect);
}			// 暫定。本来はムービーのように引数拡張して主処理へ
function button_answer3_ () {
	$("#pageRadio_53wrong"		).prop("checked", true);	// ページを移動
	// 不正解音を鳴らす
	soundPlay(audioBuffer_incorrect);
}			// 暫定。本来はムービーのように引数拡張して主処理へ
function button_answer4_ () {
	$("#pageRadio_53wrong"		).prop("checked", true);	// ページを移動
	// 不正解音を鳴らす
	soundPlay(audioBuffer_incorrect);
}			// 暫定。本来はムービーのように引数拡張して主処理へ
// endregion
// region //-- 管理メニュー ---------------------------------------------------------------------------------------

// region // 個人マスター管理 91userMaster //
function tr_user_ (id) {
	log(id);
	var idArray = id.split("_");
	var serverUserId = idArray[idArray.length - 2];
	var localUserId  = idArray[idArray.length - 1];
	//log('tr:' + serverUserId + '-' + localUserId);
	
	$('#form_serverUserId_at91').val(serverUserId);
	$('#form_localUserId_at91').val(localUserId);
	$('#form_chartNo_at91').val($('#'+id).attr('data-chartNo'));
	$('#form_lastName_at91').val($('#'+id).attr('data-familyName'));
	$('#form_firstName_at91').val($('#'+id).attr('data-givenName'));
}
function button_update_at91 () {
	// 現在選択されているものの情報
	var serverUserId	= $('#form_serverUserId_at91').val();
	var localUserId		= $('#form_localUserId_at91').val();
	var chartNo		= $('#form_chartNo_at91').val()
	var familyName		= $('#form_lastName_at91').val();
	var givenName		= $('#form_firstName_at91').val();
	
	// XML 補正
	
		// XML 読み込み
		var xmlStr = storage.getItem('settingXml');
		var xml = $.parseXML(xmlStr);
		
		// 情報更新
		var target = $(xml).find("rihasapo").find("userList").find("user[serverId="+serverUserId+"][localId="+localUserId+"]");
		//log(target);
		target.attr('chartNo', chartNo);
		target.attr('familyName', familyName);
		target.attr('givenName', givenName);
		target.attr('upd', getDateTime());
	
		// アップデート日時の更新
		$(xml).find("rihasapo").find("setting").find("update").text(getDateTime());
		
		// <rihasapo><userList> のみデバッグ出力
		log($(xml).find("userList").prop("outerHTML"));
		
		// XML 書き戻し
		var xs = new XMLSerializer();
		storage.setItem('settingXml', xs.serializeToString($(xml)[0]));
		
		// XML 送信試行
		trySendXml();
	
		
	// テーブル修正
	target = '#tr_user_' + serverUserId + '_' + localUserId;
	$(target).attr('data-chartNo', chartNo);
	$(target).attr('data-familyName', familyName);
	$(target).attr('data-givenName', givenName);
	$(target+' td').text(familyName +' '+ givenName);
}
function button_delete_at91 () {
	
	// 現在選択されているものの情報
	var serverUserId = $('#form_serverUserId_at91').val();
	var localUserId  = $('#form_localUserId_at91').val();
	
	if (localUserId != 1) {	// todo:初期ユーザは消さない？最後の1人は消せないにしようかな？
		// 選択を外す
		$('#form_serverUserId_at91').val('');
		$('#form_localUserId_at91').val('');
		
		// XML 読み込み
		var xmlStr = storage.getItem('settingXml');
		var xml = $.parseXML(xmlStr);
		
		// 情報更新
		var target = $(xml).find("rihasapo").find("userList").find("user[serverId="+serverUserId+"][localId="+localUserId+"]");
		log(target);
		target.attr('del', getDateTime());
		
		// アップデート日時の更新
		$(xml).find("rihasapo").find("setting").find("update").text(getDateTime());
		
		// <rihasapo><userList> のみデバッグ出力
		log($(xml).find("userList").prop("outerHTML"));
		
		// XML 書き戻し
		var xs = new XMLSerializer();
		storage.setItem('settingXml', xs.serializeToString($(xml)[0]));
		
		// XML 送信試行
		trySendXml();
		
		
		// テーブルから消す
		$('#tr_user_' + serverUserId + '_' + localUserId).remove();
	}
	
}
function button_insert_at91 () {
	
	// XML 読み込み
	var xmlStr = storage.getItem('settingXml');
	var xml = $.parseXML(xmlStr);
	
	// 新規作成するものの情報
	var serverUserId	= 'new';
	var nextLocalUserId	= parseInt($(xml).find("rihasapo").find("setting").find('lastLocalUserId').text(),10) + 1;
	var chartNo		= $('#form_chartNo_at91').val()
	var familyName		= $('#form_lastName_at91').val();
	var givenName		= $('#form_firstName_at91').val();
	
	// XML 情報更新
	var addXml = '';
	addXml += '<user';
	addXml += ' serverId="' + serverUserId + '"';
	addXml += ' localId="' + nextLocalUserId + '"';
	addXml += ' chartNo="' + chartNo + '"';
	addXml += ' familyName="' + familyName + '"';
	addXml += ' givenName="' + givenName + '"';
	addXml += ' upd="' + getDateTime() + '"';
	addXml += ' del=""';
	addXml += ' />';
	
	$(xml).find('rihasapo').find('userList').append(addXml);
	
	// XML アップデート日時の更新
	$(xml).find("rihasapo").find("setting").find("update").text(getDateTime());
	
	// <rihasapo><userList> のみデバッグ出力
	log($(xml).find("userList").prop("outerHTML"));
	
	// XML 書き戻し
	var xs = new XMLSerializer();
	storage.setItem('settingXml', xs.serializeToString($(xml)[0]));
	
	// XML 送信試行
	trySendXml();
	
	
	// テーブル修正
	rewriteUserList();
	
	
}
// endregion
//region // 動作モード変更 92mode //
function radio_mode_multiUser_() {			// 事業所モード
	log("radio_mode_multiUser_");
	
	XmlSettingRewrite("mode","multiUser");
	trySendXml();
}
function radio_mode_singleUser_() {			// 自宅モード
	log("radio_mode_singleUser_");
	
	XmlSettingRewrite("mode","singleUser");
	trySendXml();
}
function select_user_at92() {				//　自宅モードユーザーの選択
	log("select_user_at92 : " + $('#select_user_at92').val());
	
	// XML 読み込み
	var xmlStr = storage.getItem('settingXml');
	var xml = $.parseXML(xmlStr);
	
	var userLocalId = $('#select_user_at92').val()
	var userServerId = $(xml).children("rihasapo").children("userList").children("user[userLocalId="+userLocalId+"]").attr('userServerId');
	var familyName = $(xml).children("rihasapo").children("userList").children("user[userLocalId="+userLocalId+"]").attr('familyName');;
	var givenName = $(xml).children("rihasapo").children("userList").children("user[userLocalId="+userLocalId+"]").attr('givenName');;
	var userName = familyName + ' ' + givenName;
	
	//$(xml).children("rihasapo").children("userList").children("user").each(function(){
	//	var userServerId	= $(this).attr('userServerId'	);
	
	
	
	XmlSettingRewrite("userServerId",userServerId);
	XmlSettingRewrite("userLocalId",userLocalId);
	XmlSettingRewrite("userName",userName);
	trySendXml();
}
// endregion
// region // 個人メニュー管理 95userMenu //
function button_update_at95() {

}
function button_clear_at95() {

}
function button_history_at95() {

}
function button_prevPage_at95() {
	
	// 現在のページを取得
	var currentPage = parseInt(left($('#label_page_at95').text(),1),10);
	
	// 次に表示するページを決定
	currentPage--;
	if (currentPage < 1) {currentPage = 1;}
	
	// ページ表示を更新
	redrawPage_95userMenu(currentPage);
}
function button_nextPage_at95() {
	
	// 現在のページを取得
	var currentPage = parseInt(left($('#label_page_at95').text(),1),10);
	
	// 次に表示するページを決定
	currentPage++;
	if (3 < currentPage) {currentPage = 3;}
	
	// ページ表示を更新
	redrawPage_95userMenu(currentPage);
}
function redrawPage_95userMenu(page) {	// ページ番号に合わせて再描画
	
	// XML 読み込み
	var xmlStr = storage.getItem('settingXml');
	var xml = $.parseXML(xmlStr);
	var privateMenuXml = $(xml).find('rihasapo').children('privateMenu[userLocalId='+$('#select_user_at95').val()+']');
	log('redrawPage_95userMenu : privateMenuXml = '+privateMenuXml.prop("outerHTML"));
	
	// 再描画ループ
	for (var i=1;i<=12;i++) {
		var ii = right('00'+i,2);	// 01〜12
		
		// リハビリ名の選択
		var movieXml = $(privateMenuXml).find('movie[page='+page+'][slot='+i+']');
		log('movieId = ' + $(movieXml).attr('movieId'));
		$('#select_userMenu'+ii+'_rehabili').val($(movieXml).attr('movieId'));
		
		// 回数の選択
		$('#select_userMenu'+ii+'_count').val($(movieXml).attr('count'));
		
		// 結果の出力
		$('#text_userMenu'+ii+'_result').val($(movieXml).attr('result'));
	}
	
	// ページ表示を更新
	$('#label_page_at95').text(page+'/3');
	
	
}

function select_user_at95 () {
	// log("check");
	redrawPage_95userMenu(1);
}

function select_userMenu01_rehabili(val) {select_userMenuXX_rehabili( 1,val)}
function select_userMenu02_rehabili(val) {select_userMenuXX_rehabili( 2,val)}
function select_userMenu03_rehabili(val) {select_userMenuXX_rehabili( 3,val)}
function select_userMenu04_rehabili(val) {select_userMenuXX_rehabili( 4,val)}
function select_userMenu05_rehabili(val) {select_userMenuXX_rehabili( 5,val)}
function select_userMenu06_rehabili(val) {select_userMenuXX_rehabili( 6,val)}
function select_userMenu07_rehabili(val) {select_userMenuXX_rehabili( 7,val)}
function select_userMenu08_rehabili(val) {select_userMenuXX_rehabili( 8,val)}
function select_userMenu09_rehabili(val) {select_userMenuXX_rehabili( 9,val)}
function select_userMenu10_rehabili(val) {select_userMenuXX_rehabili(10,val)}
function select_userMenu11_rehabili(val) {select_userMenuXX_rehabili(11,val)}
function select_userMenu12_rehabili(val) {select_userMenuXX_rehabili(12,val)}
function select_userMenuXX_rehabili(slot,movieId) {
	log("select_userMenuXX_rehabili("+ slot +",'"+movieId+"')");
	log(movieId);
	// XML 読み込み
	var xmlStr = storage.getItem('settingXml');
	var xml = $.parseXML(xmlStr);
	
	// 新規作成もしくは更新する情報
	var XX = right("00" + slot,2);
	// 選択が空白の場合はカウントとリザルトをリセット
	if (movieId=='') {
		$('#select_userMenu'+XX+'_count').val('');
		$('#text_userMenu'+XX+'_result').val('');
	} else {
		if ($('#select_userMenu'+XX+'_count').val()==null) {
			$('#select_userMenu'+XX+'_count').val('10');
		}
		$('#text_userMenu'+XX+'_result').val('未実施');
	}
	var userLocalId	= $('#select_user_at95').val();
	var page = ($('#label_page_at95').text()).split('/')[0];
	var count = $('#select_userMenu'+XX+'_count').val();
	var result = $('#text_userMenu'+XX+'_result').val();
	
	var pvMenu = '';
	pvMenu += '<movie';
	pvMenu += ' page="'+page+'"';
	pvMenu += ' slot="'+slot+'"';
	pvMenu += ' movieId="'+movieId+'"';
	pvMenu += ' latestVersion=""';
	pvMenu += ' count="'+count+'"';
	pvMenu += ' result="'+result+'"';
	pvMenu += '/>';
	
	// もし既存のタグが存在しているなら削除した上で追加
	if ($(xml).find('rihasapo').find('privateMenu[userLocalId="'+userLocalId+'"]').find('movie[page="'+page+'"][slot="'+slot+'"]').length) {
		$(xml).find('rihasapo').find('privateMenu[userLocalId="'+userLocalId+'"]').find('movie[page="'+page+'"][slot="'+slot+'"]').remove();
	}
	$(xml).find('rihasapo').find('privateMenu[userLocalId="' + userLocalId + '"]').append(pvMenu);
	
	// XML アップデート日時の更新
	$(xml).find("rihasapo").find("setting").find("update").text(getDateTime());
	
	// <rihasapo><userList> のみデバッグ出力
	log($(xml).find('privateMenu[userLocalId="'+userLocalId+'"]').prop("outerHTML"));
	
	// XML 書き戻し
	var xs = new XMLSerializer();
	storage.setItem('settingXml', xs.serializeToString($(xml)[0]));
	
	// XML 送信試行
	trySendXml();
}

function select_userMenu01_count(val) {select_userMenuXX_count( 1,val)}
function select_userMenu02_count(val) {select_userMenuXX_count( 2,val)}
function select_userMenu03_count(val) {select_userMenuXX_count( 3,val)}
function select_userMenu04_count(val) {select_userMenuXX_count( 4,val)}
function select_userMenu05_count(val) {select_userMenuXX_count( 5,val)}
function select_userMenu06_count(val) {select_userMenuXX_count( 6,val)}
function select_userMenu07_count(val) {select_userMenuXX_count( 7,val)}
function select_userMenu08_count(val) {select_userMenuXX_count( 8,val)}
function select_userMenu09_count(val) {select_userMenuXX_count( 9,val)}
function select_userMenu10_count(val) {select_userMenuXX_count(10,val)}
function select_userMenu11_count(val) {select_userMenuXX_count(11,val)}
function select_userMenu12_count(val) {select_userMenuXX_count(12,val)}
function select_userMenuXX_count(slot,count) {
	
	// XML 読み込み
	var xmlStr = storage.getItem('settingXml');
	var xml = $.parseXML(xmlStr);
	
	// 新規作成もしくは更新する情報
	
	var XX = right("00" + slot,2);
	var userLocalId	= $('#select_user_at95').val();
	var page = ($('#label_page_at95').text()).split('/')[0];
	var movieId = $('#select_userMenu'+XX+'_rehabili').val();
	
	var pvMenu = '';
	pvMenu += '<movie';
	pvMenu += ' page="'+page+'"';
	pvMenu += ' slot="'+slot+'"';
	pvMenu += ' movieId="'+movieId+'"';
	pvMenu += ' latestVersion=""';
	pvMenu += ' count="'+count+'"';
	pvMenu += ' result="未実施"';
	pvMenu += '/>';
	
	// もし既存のタグが存在しているなら削除した上で追加
	if ($(xml).find('rihasapo').find('privateMenu[userLocalId="'+userLocalId+'"]').find('movie[page="'+page+'"][slot="'+slot+'"]').length) {
		$(xml).find('rihasapo').find('privateMenu[userLocalId="'+userLocalId+'"]').find('movie[page="'+page+'"][slot="'+slot+'"]').remove();
	}
	$(xml).find('rihasapo').find('privateMenu[userLocalId="' + userLocalId + '"]').append(pvMenu);
	
	// XML アップデート日時の更新
	$(xml).find("rihasapo").find("setting").find("update").text(getDateTime());
	
	// <rihasapo><userList> のみデバッグ出力
	log($(xml).find('privateMenu[userLocalId="'+userLocalId+'"]').prop("outerHTML"));
	
	// XML 書き戻し
	var xs = new XMLSerializer();
	storage.setItem('settingXml', xs.serializeToString($(xml)[0]));
	
	// XML 送信試行
	trySendXml();
}

// endregion
// region // ドリルマスター管理 97drillMaster //



// endregion
// region // 個人ドリルリスト管理 98userDrill //



// endregion


// endregion


// region ////////////////////////////////////// 単純なページ移動 //////////////////////////////////////////
function button_go_01top_ () {
	$("#pageRadio_01top"		).prop("checked", true);	// ページを移動
}			// 単純なページ移動
function button_go_11rihasapoTop_ () {
	$("#pageRadio_11rihasapoTop"	).prop("checked", true);	// ページを移動
}		// 単純なページ移動
function button_go_20userMenu_ () {
	redrawPage_20userMenu(1);					// ページ指定
	$("#pageRadio_20userMenu"	).prop("checked", true);	// ページを移動
}			// 単純なページ移動
function button_go_30movieList1_ () {
	// 戻るときに使う
	$("#pageRadio_30movieList1"	).prop("checked", true);	// ページを移動
}		// 単純なページ移動
function button_go_31movieList2_ () {
	// 戻るときに使う
	$("#pageRadio_31movieList2"	).prop("checked", true);	// ページを移動
}		// 単純なページ移動
function button_go_50drillTop_ () {
	$("#pageRadio_50drillTop"	).prop("checked", true);	// ページを移動
}			// 単純なページ移動
function button_go_51question_ () {
	$("#pageRadio_51question"	).prop("checked", true);	// ページを移動
}			// 単純なページ移動
function button_go_90adminTop_ () {
	$("#pageRadio_90adminTop"	).prop("checked", true);	// ページを移動
}			// 単純なページ移動
function button_go_91userMaster_ () {
	$("#pageRadio_91userMaster"	).prop("checked", true);	// ページを移動
}		// 単純なページ移動
function button_go_92mode_ () {
	$("#pageRadio_92mode"		).prop("checked", true);	// ページを移動
}			// 単純なページ移動
function button_go_93password_ () {
	$("#pageRadio_93password"	).prop("checked", true);	// ページを移動
}			// 単純なページ移動
function button_go_94rehabiliMaster_ () {
	$("#pageRadio_94rehabiliMaster"	).prop("checked", true);	// ページを移動
}		// 単純なページ移動
function button_go_95userMenu_ () {
	redrawPage_95userMenu(1);					// ページ指定
	$("#pageRadio_95userMenu"	).prop("checked", true);	// ページを移動
}			// 単純なページ移動
function button_go_96normalMenu_ () {
	$("#pageRadio_96normalMenu"	).prop("checked", true);	// ページを移動
}		// 単純なページ移動
function button_go_97drillMaster_ () {
	$("#pageRadio_97drillMaster"	).prop("checked", true);	// ページを移動
}		// 単純なページ移動
function button_go_98userDrill_ () {
	$("#pageRadio_98userDrill"	).prop("checked", true);	// ページを移動
}		// 単純なページ移動
// endregion

// region //////////////////////////////////////// 汎用関数 ///////////////////////////////////////////////////
function replaceAll(a,b,c) {
	return a.split(b).join(c);
}			// 文字列の置換
function objectArrayIndexOf(ary, key, val) {
	var ret = -1;
	for (var i=0; i<ary.length; i++) {
		log("判断" + ary[i][key]);
		if (ary[i][key] == val) {
			ret = i;
			break;
		}
	}
	return ret;
}	// 配列の中から値の等しいもののインデックス番号を返す
function objectArraySort(ary, key, order) {
	//--------------------------------------------
	// Sort Object's Array by key
	//   ary   : Array [{},{},{}...]
	//   key   : string
	//   order : string "asc"(default) or "desc"
	//--------------------------------------------
	var reverse = 1;
	if(order && order.toLowerCase() == "desc")
		reverse = -1;
	ary.sort(function(a, b) {
		if(a[key] < b[key])
			return -1 * reverse;
		else if(a[key] == b[key])
			return 0;
		else
			return 1 * reverse;
	});
}		// 配列のソート
function left(str, n){
	if (n <= 0)
		return "";
	else if (n > String(str).length)
		return str;
	else
		return String(str).substring(0,n);
}				// 文字列から左n文字
function right(str, n){
	if (n <= 0)
		return "";
	else if (n > String(str).length)
		return str;
	else {
		var iLen = String(str).length;
		return String(str).substring(iLen, iLen - n);
	}
}				// 文字列から右n文字
var data2buffer = function(data) {	// data url scheme を buffer に変換
	var byteString = atob(data.split(',')[1])
	var len = byteString.length;
	var buffer = new Uint8Array(len);
	for (var i=0; i<len; ++i) {
		buffer[i] = byteString.charCodeAt(i);
	}
	return buffer.buffer;
};			// data url scheme を buffer に変換（効果音に使用）
function soundPlay(buf) {
	// サウンドを鳴らす
	var source = context.createBufferSource();
	source.buffer = buf;
	source.connect(context.destination);
	source.start(0);
}				// 効果音再生（Web Audio API）
function XmlSettingRewrite(key,val) {
	
	// XML 読み込み
	var xmlStr = storage.getItem('settingXml');
	var xml = $.parseXML(xmlStr);
	
	// XML 更新
	$(xml).find("rihasapo").find("setting").find(key).text(val);
	
	// アップデート日時の更新
	$(xml).find("rihasapo").find("setting").find("update").text(getDateTime());
	
	// <rihasapo><setting> のみデバッグ出力
	log($(xml).find("rihasapo").find("setting").prop("outerHTML"));
	
	// XML 書き戻し
	var xs = new XMLSerializer();
	storage.setItem('settingXml', xs.serializeToString($(xml)[0]));
	
	// XML 送信試行
	// trySendXml();
}
function getDateTime() {
	var tempDate = new Date();
	var Y  = tempDate.getFullYear();
	var Mo = tempDate.getMonth() + 1;
	var D  = tempDate.getDate();
	var H  = tempDate.getHours();
	var Mi = tempDate.getMinutes();
	var S  = tempDate.getSeconds();
	var Ms = tempDate.getMilliseconds();
	
	var YYYY = right("0000" + Y, 4);
	var MM   = right("00" + Mo , 2);
	var DD   = right("00" + D  , 2);
	var HH   = right("00" + H  , 2);
	var MI   = right("00" + Mi , 2);
	var SS   = right("00" + S  , 2);
	var MSS  = right("000" + Ms, 3);
	
	//return YYYY +"-"+ MM +"-"+ DD +" "+ HH +":"+ MI +":"+ SS +"."+ MSS;
	return YYYY + MM + DD + HH + MI + SS + MSS;
}
function trySendXml () {
	// log("trySendXml : trySendXml でXMLを送信");
	
	// XML 読み込み
	var xmlStr = storage.getItem('settingXml');
	
	$.ajax('https://www.bianconero.co.jp/dev/goodlife/www/api/14_uploadXml.php',{
			type: 'post'
			, dataType: 'text'	// 一度ローカルストレージに保存するのでわざとtext
			, cache: false
			, data: {
				"xml":xmlStr
			}
		})
		.done(function(data) {
			
			log("trySendXml : XML 送信成功");
			log("trySendXml : " + data.toString());
			
			
			//storage.setItem('settingXml', data);
			//useridというキーの値を削除する場合
			//storage.removeItem('userid');
			//ストレージに保存されているデータをすべてクリアする場合
			//storage.clear();
			
			// いる　initializeAll();
			
			
			// log("XML:" + data.toString());
			
		})
		.fail(function(data, textStatus, errorThrown) {
			//alert(textStatus); //エラー情報を表示
			log("trySendXml : XML 送信失敗失敗");
			//log(errorThrown.message); //例外情報を表示
			log(errorThrown.toString()); //例外情報を表示
			
			// いる　initializeAll();
		})
		.always(function(data, textStatus, returnedObject) {
			// ajaxの通信に成功した場合はdone()と同じ、失敗した場合はfail()と同じ引数を返す
			// alert(textStatus);
			
		});
}
// endregion

// region //////////////////////////////////////// App 用コード ////////////////////////////////////////////

function externalGetUUID(nextFunctionName){
	
	if (ua.isiOSApp) {
		window.webkit.messageHandlers.getUUID.postMessage(nextFunctionName); //引数はコールバック関数を文字列で指定
	} else {
		// もし id と同じ関数名があるならその関数をキック
		if ( eval("typeof " + nextFunctionName + ' == "function"') )  {
			eval(nextFunctionName + "('dummyUUID')");
		}
	}
}		// デバイスUUIDを取得
function log(str) {			// 省略関数名
	externalLog(str);
}					// externalLog のエイリアス（普段はこちらを使う）
function externalLog(str){		// 本来の関数名
	if (ua.isiOSApp) {
		window.webkit.messageHandlers.print.postMessage(str); //引数は出力文字列
	} else {
		console.log(str);
	}
}				// ブラウザではコンソールへ、アプリではXcodeへ出力
function externalCopyTmp(){
	if (ua.isiOSApp) {
		log("externalCopyTmp");
		window.webkit.messageHandlers.copyTmp.postMessage("kuroishi");	//メッセージはなにかしら送らなければいけないみたいだ
	} else {
		// 何もしない
	}
}				// バンドルのtmp内全ファイルをアプリtmpへコピー
function externalGetTmpPath(nextFunctionName){
	if (ua.isiOSApp) {
		window.webkit.messageHandlers.getTmpPath.postMessage(nextFunctionName); //引数はコールバック関数を文字列で指定
	} else {
		// もし id と同じ関数名があるならその関数をキック
		if ( eval("typeof " + nextFunctionName + ' == "function"') )  {
			eval(nextFunctionName + "('tmp/')");
		}
	}
}	// アプリtmpのパスを取得
function externalGetTmpFileList(nextFunctionName){
	if (ua.isiOSApp) {
		window.webkit.messageHandlers.getTmpFileList.postMessage(nextFunctionName); //引数はコールバック関数を文字列で指定
	}
}
function externalDownloadFilesToTmp(fileArray) {
    
//    let sampleArray = ['143a-01.mp4', '143b-01.mp4']
////    let sampleArray = ['IMG_0199.jpg']
//
    if (ua.isiOSApp) {
//        var data = JSON.stringify({
//            onProgress: 'downloadProgress'
//            , onComplete: 'downloadComplete'
//            , onError: 'downloadError'
//            , serverDirectory: 'http://www.bianconero.co.jp/dev/goodlife/media/movie/'
//            , downloadFiles: sampleArray
//        });
    
 //   let sampleArray = ['IMG_0199.jpg', 'IMG_0202.jpg', 'IMG_0203.jpg', 'IMG_0205.jpg', 'IMG_0206.jpg', 'IMG_0207.jpg', 'IMG_0208.jpg', 'IMG_0301.jpg', 'IMG_0302.jpg', 'IMG_0303.jpg', 'IMG_0304.jpg', 'IMG_0306.jpg', 'IMG_0308.jpg', 'IMG_0313.jpg', 'IMG_0315.jpg', 'IMG_0317.jpg', 'IMG_0318.jpg', 'IMG_0319.jpg', 'IMG_0320.jpg', 'IMG_0321.jpg', 'IMG_0322.jpg', 'IMG_0324.jpg', 'IMG_0325.jpg', 'IMG_0326.jpg', 'IMG_0327.jpg', 'IMG_0328.jpg', 'IMG_0330.jpg', 'IMG_0331.jpg', 'IMG_0336.jpg', 'IMG_0338.jpg', 'IMG_0340.jpg', 'IMG_0341.jpg', 'IMG_0342.jpg', 'IMG_0343.jpg', 'IMG_0344.jpg', 'IMG_0345.jpg', 'IMG_0346.jpg', 'IMG_0349.jpg', 'IMG_0352.jpg', 'IMG_0353.jpg', 'IMG_0355.jpg', 'IMG_0356.jpg', 'IMG_0357.jpg', 'IMG_0359.jpg', 'IMG_0360.jpg', 'IMG_0361.jpg', 'IMG_0363.jpg', 'IMG_0364.jpg', 'IMG_0365.jpg', 'IMG_0463.jpg', 'IMG_0464.jpg', 'IMG_0472.jpg', 'IMG_0473.jpg', 'IMG_0474.jpg', 'IMG_0475.jpg', 'IMG_0476.jpg', 'IMG_0477.jpg', 'IMG_0478.jpg', 'IMG_0479.jpg', 'IMG_0480.jpg', 'IMG_0481.jpg', 'IMG_0485.jpg', 'IMG_0486.jpg', 'IMG_0487.jpg', 'IMG_0488.jpg', 'IMG_0489.jpg', 'IMG_0490.jpg', 'IMG_0491.jpg', 'IMG_0492.jpg', 'IMG_0496.jpg', 'IMG_0519.jpg', 'IMG_0527.jpg', 'IMG_0528.jpg', 'IMG_0529.jpg', 'IMG_0531.jpg', 'IMG_0532.jpg', 'IMG_0533.jpg', 'IMG_0535.jpg', 'IMG_0537.jpg', 'IMG_0538.jpg', 'IMG_0539.jpg', 'IMG_0540.jpg', 'IMG_0541.jpg', 'IMG_0542.jpg', 'IMG_0543.jpg', 'IMG_0545.jpg', 'IMG_0546.jpg', 'IMG_0547.jpg', 'IMG_0548.jpg', 'IMG_0549.jpg', 'IMG_0550.jpg', 'IMG_0553.jpg', 'IMG_0554.jpg', 'IMG_0557.jpg', 'IMG_0558.jpg', 'IMG_0559.jpg', 'IMG_0560.jpg', 'IMG_0561.jpg', 'IMG_0562.jpg', 'IMG_0568.jpg', 'IMG_0569.jpg', 'IMG_0571.jpg', 'IMG_0573.jpg', 'IMG_0581.jpg', 'IMG_0591.jpg', 'IMG_0596.jpg', 'IMG_0605.jpg', 'IMG_0670.jpg', 'IMG_0671.jpg', 'IMG_0672.jpg', 'IMG_0673.jpg', 'IMG_0685.jpg', 'IMG_0686.jpg', 'IMG_0689.jpg', 'IMG_0692.jpg'];
//        let sampleArray = ['IMG_0671.jpg'];
        var data = JSON.stringify({
                                  onProgress: 'downloadProgress',
                                  onComplete: 'downloadComplete',
                                  onError: 'downloadError',
                                  serverDirectory: 'https://www.bianconero.co.jp/dev/goodlife/media/movie/',
                                  downloadFiles: fileArray
                                  });
        //'http://www.bianconero.co.jp/shiraishi/'
        //'http://www.bianconero.co.jp/dev/goodlife/media/movie/'
        
        window.webkit.messageHandlers.downloadFilesToTmp.postMessage(data); //引数はコールバック関数を文字列で指定

	}
} // 配列で指定したファイルをダウンロード
function externalCancelDownloadTasks() {
    window.webkit.messageHandlers.cancelDownloadTasks.postMessage(null);
} //tshiraishi ダウンロードのキャンセル
function externalGetReceipts(_secret) {

    if (ua.isiOSApp) {

        let prodidArray = [] //未使用だが空で渡している。
        var data = JSON.stringify({
            onCallBack: 'onGetReceipts',
            onError: 'onErrorGetReceipts',
            productIds: prodidArray,
            secret: _secret
        });
        window.webkit.messageHandlers.getReceipts.postMessage(data);

    }
} //tshiraishi レシートを確認するもの。Appleが発行したレシートをそのまま返す（自動更新購読タイプを購入していることが必要。未購入の場合は無反応となる。）
function externalGetLicenses(_secret) {

    if (ua.isiOSApp) {

    	// var prodidArray = [
        //	// "osaka.goodlife.rihasapo.individual.basic.OneYear",
	//	"osaka.goodlife.rihasapo.individual.basic.Month",
	//	"osaka.goodlife.rihasapo.individual.add02",
	//	"osaka.goodlife.rihasapo.individual.add03",
	//	"osaka.goodlife.rihasapo.individual.add06",
	//	"osaka.goodlife.rihasapo.individual.add07",
	//	"osaka.goodlife.rihasapo.individual.add08"
	// ] //["osaka.goodlife.rihasapo.individual.basic.OneYear"] //["osaka.goodlife.rihasapo.individual.basic.Month"]
	
	// var prodidArray = [
	// 	"individual1Month",
	// 	"movie057",
	// 	"movie068",
	// 	"movie069",
	// 	"movie070",
	// 	"movie091",
	// 	"movie092",
	// 	"movie093",
	// 	"movie108",
	// 	"movie109",
	// 	"movie116",
	// 	"movie119",
	// 	"movie120",
	// 	"movie121",
	// 	"movie141",
	// 	"movie142",
	// 	"movie143"
	// ]

        var prodidArray = ["individual1Month"];
	    
	    var data = JSON.stringify({
            onCallBack: 'onGetLicenses',
            onError: 'onErrorGetLicenses',
            productIds: prodidArray,
            secret: _secret
        });
        window.webkit.messageHandlers.getLisences.postMessage(data);

    }
} //tshiraishi ライセンスを確認するもの。アプリ内課金のレシートを確認し、指定したライセンスリストのそれぞれのライセンス状況がどうなっているかをJSONでコールバックさせる（予定）。
function externalBuyProduct(_userid, _prodidArray){
    if (ua.isiOSApp) {

        var data = JSON.stringify({
            onCallBack: 'onBuyProduct',
            onError: 'onErrorBuyProduct',
            productIds: _prodidArray,
            userid: _userid
        });
       window.webkit.messageHandlers.buyProduct.postMessage(data);

    }
} //tshiraishi 指定したプロダクトを購入するメソッド。もし永久ライセンスを持っているものを指定した場合でも購入処理を開始するが、その場合、iTunes側からすでに購入済みのため再購入するかという旨のメッセージが表示される。
function externalRestoreProducts(_userid){
    if (ua.isiOSApp) {

        var data = JSON.stringify({
            onCallBack: 'onRestoreProducts',
            onError: 'onErrorRestoreProducts',
            productIds: [],
            userid: _userid
        });
        window.webkit.messageHandlers.restoreProducts.postMessage(data);

    }
} //tshiraishi 指定したプロダクトを購入するメソッド。もし永久ライセンスを持っているものを指定した場合でも購入処理を開始するが、その場合、iTunes側からすでに購入済みのため再購入するかという旨のメッセージが表示される。


// 以下、Delegate（コールバック関数）

function downloadProgress(id, val){
	// id にはパスを含まないファイル名（配列で指定したもの）が入ってくる
	// val は小数で 0.53 とか（53%の意）単一ファイルのダウンロード率が入ってくる
	
	// externalLog("downloadProgress:" + id + " = " + val);
	
	$('div#bar_oneFile_at99').css('width',parseInt(700*val,10)+'px');
}
function downloadComplete(id, val){
	// id にはパスを含まないファイル名（配列で指定したもの）が入ってくる
	// val は小数で 0.53 とか（53%の意）全ファイルのダウンロード率が入ってくる
	
	// externalLog("downloadComplete:" + id + " = done");


	$('div#bar_files_at99').css('width',parseInt(700*val,10)+'px');
	
//    if (0.99999 < val) {
//        // 最終的に 1 は返ってこず、0.999999999999997　とかが返る。
//        // １つ前が 0.991869918699184 だったのでこれぐらいにしとくか。
    if (val == 1.0){ //1.0を返すように修正 //tshiraishi
		downloadAllContents_end();
	}
	
	
	
}
function callMskg(arg, arg2){
	// 呼び方：callMskg('JSから呼び出し',1.234);
	var data = JSON.stringify({
		message: arg, num: arg2
	});
	window.webkit.messageHandlers.Mskg.postMessage(data);
}			// 白石用コード。複数の値を受け渡す例。


function onGetReceipts(arg){
    var decodedStr = decodeURI(arg);
    var json = JSON.parse(decodedStr);

	//jsonのサンプル http://jsoneditoronline.org/?id=d49846223bd97f6857a31f61f2951264
	//自動継続のプロダクトは、最初に購入したものからすべての履歴が取得される。
	for(var i=0; i<json.length; i++){
        log('onGetReceipts : product_id : ' + json[i]["product_id"]);
        log('onGetReceipts : expires_date : ' + json[i]["expires_date"]);
	}

	 //### print : onGetReceipts : 2018-03-24 10:19:52 Etc/GMT
	
	
	
	
}
function onErrorGetReceipts(arg){
    log("onErrorGetReceipts : " + arg);
}

function onGetLicenses(arg){
    var decodedStr = decodeURI(arg);
    var json = JSON.parse(decodedStr);

    log('onGetLicenses : ' + decodedStr);

    // 黒石コメント化　externalGetReceipts(SECRET); //購入済み後、即座にレシートを確認。

    //### print : onGetLicenses : 2018-03-24 10:19:52 Etc/GMT
	
	// 黒石コメント化　externalGetReceipts(SECRET); //白石：自動更新確認用レシートログ（「onGetReceipts : 」でログをフィルタすると見やすくなり、期限切れ日時を確認できる）
	
	// ここから黒石追加
	// ライセンス無効＆再購入ダイアログの非表示
	$("#dialog_99expired").hide();
    $("#dialog_100expired").hide();
	onLoad_4(json);	// 処理を戻す
	
	
}
function onErrorGetLicenses(arg){
    log("onErrorGetLicenses : " + arg);
	
	
	// ここから黒石追加
	// エラーが出ても onGetLicenses が全部 false で帰ってくるので、ここは無視　　onLoad_4();	// 処理を戻す
 
}


function onBuyProduct(arg){
    log("onBuyProduct : " + arg);

    //argはJSONにしました。JSON作成時にエラーが発生したときだけ"error"という文字列。
    if (arg != "error"){
        var buyProdJson = JSON.parse(decodeURI(arg));
        log("onBuyProduct -> type : " + buyProdJson["type"]);
        log("onBuyProduct -> prodid : " + buyProdJson["prodid"]);
        log("onBuyProduct -> username : " + buyProdJson["username"]);
    }



    
    // もし現在の画面がムービー画面だったら続きを再生可能にしてしまおう（応急処置）
    if ($("#pageRadio_40movie").prop("checked")) {
	
	
	$("div[id^=title_movie]").css('display', 'block');		// 「再生する回数を選んで下さい」 表示
	$("div[id^=title_purchase]").css('display', 'none');		// 「課金動画です購入して下さい」 非表示
	
	$("div#button_purchase120").css('display', 'none');		// 「購入ボタン」 非表示
	
	$("div[id^=button_play01z05_]").css('display', 'none');		// 01〜05 非表示
	$("div[id^=button_play10z50_]").css('display', 'block');	// 01〜05 表示

	   
	   
    }
    
    
    externalGetLicenses(SECRET); //購入済み後、即座にライセンスを確認。
}
function onErrorBuyProduct(arg){
    log("onErrorBuyProduct : " + arg);
	
	// ライセンス無効＆再購入ダイアログの表示
	$("#overlay_background_at99").show();
	$("#dialog_99purchaseError").show();
	
	log("onErrorBuyProduct : finished");
}

function onRestoreProducts(arg){
    log("onRestoreProducts : " + arg);

    //argはJSONにしました。JSON作成時にエラーが発生したときだけ"error"という文字列。
    if (arg != "error"){
        var RestoreProdsJson = JSON.parse(decodeURI(arg));
        log("onRestoreProducts -> type : " + RestoreProdsJson["type"]);
        log("onRestoreProducts -> prodid : " + RestoreProdsJson["prodid"]);
        log("onRestoreProducts -> username : " + RestoreProdsJson["username"]);
    }


    // もし現在の画面がムービー画面だったら続きを再生可能にしてしまおう（応急処置）
    // if ($("#pageRadio_40movie").prop("checked")) {
    //
    //
    //     $("div[id^=title_movie]").css('display', 'block');		// 「再生する回数を選んで下さい」 表示
    //     $("div[id^=title_purchase]").css('display', 'none');		// 「課金動画です購入して下さい」 非表示
    //
    //     $("div#button_purchase120").css('display', 'none');		// 「購入ボタン」 非表示
    //
    //     $("div[id^=button_play01z05_]").css('display', 'none');		// 01〜05 非表示
    //     $("div[id^=button_play10z50_]").css('display', 'block');	// 01〜05 表示
    //
    //
    //
    // }

	log("リストアしました。");
    restoreFlag = true;
    externalGetLicenses(SECRET); //最後のリストア後、即座にライセンスを確認。
}
function onErrorRestoreProducts(arg){
    log("onErrorRestoreProducts : " + arg);

    //todo 白石コメント：リストア用なので以下コメントアウトしておきます。必要かどうかご確認を。
    // ライセンス無効＆再購入ダイアログの表示
    // $("#overlay_background_at99").show();
    // $("#dialog_99purchaseError").show();

    log("onErrorRestoreProducts : finished");
}

// endregion
