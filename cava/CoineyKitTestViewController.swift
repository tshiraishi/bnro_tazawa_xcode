//
//  CoineyKitTestViewController.swift
//  cava
//
//  Created by Tetsuya Shiraishi on 2019/02/04.
//  Copyright © 2019年 MP, K.K. All rights reserved.
//

import UIKit
import CoineyKit

class CoineyKitTestViewController: UIViewController, CYCoineyViewControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBOutlet weak var productNameField: UITextField!
    @IBOutlet weak var productPriceField: UITextField!
    
    @IBAction func makePayment(sender: AnyObject)
    {
        let memo = productNameField.text ?? ""
        let amount = Int64(productPriceField.text!) ?? 0
        
        // CYCoineyViewController のインスタンスを作成
        //        let coineyController = CYCoineyViewController.init(amount: amount, memo: memo)
        let coineyController = CYCoineyViewController.init(amount: amount, memo: memo, paymentMethod: CYPaymentMethod.creditCard)
        
        
        
        coineyController.delegate = self
        //        guard let coineyController = CYCoineyViewController.init(amount: amount, memo: memo) else {
        //            fatalError("Failed to initialize CYCoineyViewController.")
        //        }
        
        // ViewController の上に表示
        self.present(coineyController, animated: true, completion: nil)
    }
    
    func coineyViewControllerDidCancel(_ aController: CYCoineyViewController) {
        self.dismiss(animated: true, completion: nil)
        print("Cancelled payment.")
    }
    
    func coineyViewController(_ aController: CYCoineyViewController, didComplete aTransaction: CYTransaction) {
        print("Completed transaction: \(String(describing: aTransaction))")
        self.dismiss(animated: true, completion: nil)
    }

}
