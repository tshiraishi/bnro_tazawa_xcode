//
//  CouponTableViewController.swift
//  cava
//
//  Created by Tetsuya Shiraishi on 2019/03/19.
//  Copyright © 2019年 MP, K.K. All rights reserved.
//

import UIKit
import GTMSessionFetcher
import ObjectMapper
import CoreImage

class CouponTableObj: Mappable {
    var id:String?
    var name:String?
    var created:String?
    var modified:String?
    var code:String?
    var item_id:String?
    var ignoreOrder:String?
    var start:String?
    var end:String?
    var activity:String?
    //Item
    var itemName:String?
    var itemShortName:String?
    var itemPrice:String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id <- map["Coupon.id"]
        name <- map["Coupon.name"]
        created <- map["Coupon.created"]
        modified <- map["Coupon.modified"]
        code <- map["Coupon.code"]
        item_id <- map["Coupon.item_id"]
        ignoreOrder <- map["Coupon.ignoreOrder"]
        start <- map["Coupon.start"]
        end <- map["Coupon.end"]
        activity <- map["Coupon.activity"]
        
        itemName <- map["Item.name"]
        itemShortName <- map["Item.shortname"]
        itemPrice <- map["Item.price"]
    }
}

class CouponTableResults: Mappable {
    var error:Int?
    var result:Array<CouponTableObj>?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        error <- map["error"]
        result <- map["result"]
    }
}

class CouponTableViewController: UITableViewController {

    var iCouponTableList:[CouponTableObj] = []
    var iAPIServer = "http://tazawa-tori.bianconero.co.jp/dev/tazawa/coupons/listCoupons"
    var iFetcher:GTMSessionFetcher?
    var iRefresh:UIRefreshControl!
    
    @IBAction func closeBtnAction(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        self.iRefresh = UIRefreshControl()
        self.iRefresh.attributedTitle = NSAttributedString(string: "更新")
        self.iRefresh.addTarget(self, action: "refreshData", for: UIControl.Event.valueChanged)
        self.tableView.addSubview(self.iRefresh)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.refreshData(_:)), name: Notification.Name(rawValue:"refreshCouponTable"), object: nil)
        
        //いきなりイベント発生させて呼ぶ
        NotificationCenter.default.post(name: Notification.Name(rawValue:"refreshCouponTable"), object: nil, userInfo: nil)
        
    }

    @objc public func refreshData(_ notification: NSNotification){
        let params: [String: String] = [
            "pass": "shoothahazusanai"//,
            //            "location": "\(pLat),\(pLng)", //"35.703528,139.560737",
            //            "radius": "\(radius)", //"100",
            //            "type": type ?? "", //"restaurant",
            //            "keyword": keyword ?? ""
        ]
        print(params)
        
        let paramString = params.stringFromHttpParameters()
        let requestURL = URL(string:"\(iAPIServer)?\(paramString)")!
        
        var request = URLRequest(url: requestURL)
        request.httpMethod = "GET"
        
        print(request.url)
        
        self.iFetcher = GTMSessionFetcher.init(request: request)
        
        //        if let _self = self.iSelf{
        //            self.iHud.show(in: _self.view)
        //        }
        
        self.iFetcher?.beginFetch(completionHandler: { (data:Data?, error:Error?) in
            //self.iHud.dismiss()
            
            
            if let _data = data{
                
                if let jsonStr = String(data: _data, encoding: String.Encoding.utf8){
                    if let _ = jsonStr.data(using: .utf8){
                        
                        do {
                            let jsonobj = Mapper<CouponTableResults>().map(JSONString: jsonStr)
                            print(jsonobj)
                            //NotificationCenter.default.post(name: Notification.Name(rawValue:"onGetPlaces"), object: nil, userInfo: ["results" : jsonobj?.results])
                            
                            //                                print(jsonobj)
                            
                            
                            //jsonobj?.entities?.count
                            if let _result = jsonobj?.result {
                                self.iCouponTableList = _result
                                self.tableView.reloadData()
                            }
                            
                            
                        } catch {
                            print(error.localizedDescription)
                            
                        }
                        
                        self.iRefresh.endRefreshing()
                    }
                }
                
            }else{
                print("data is empty....")
                
            }
            
            
        })
    }

    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return iCouponTableList.count
    }

    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90.0
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CouponTableReuseIdentifier", for: indexPath) as! CouponTableViewCell

        // Configure the cell...
        let _coupon = iCouponTableList[indexPath.row]
        
        cell.iName.text = _coupon.name
        cell.iShortName.text = "code=" + (_coupon.code ?? "no")
        cell.iStartDateTimeButton.setTitle((_coupon.start ?? "未設定"), for: UIControl.State.normal)
        cell.iEndDateTimeButton.setTitle((_coupon.end ?? "未設定"), for: UIControl.State.normal)
        cell.iActivitySwitch.isOn = (_coupon.activity == "1")
        
        cell.iCouponID = _coupon.id
        
        cell.iQRImage.image = createQRCode(message: "cp:" + (_coupon.code ?? "no"), correctionLevel: "M", moduleSize: 3)
        
        cell.iVc = self
        
        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    func createQRCode(message: String, correctionLevel: String = "M", moduleSize: CGFloat = 1) -> UIImage {
        
        let dat = message.data(using: String.Encoding.utf8)!
        
        let qr = CIFilter(name: "CIQRCodeGenerator", parameters: [
            "inputMessage": dat,
            "inputCorrectionLevel": correctionLevel,
            ])!
        
        // moduleSize でリサイズ
        let sizeTransform = CGAffineTransform(scaleX: moduleSize, y: moduleSize)
        let ciImg = qr.outputImage!.transformed(by: sizeTransform)
        
        return UIImage(ciImage: ciImg, scale: 1, orientation: .up)
    }
}
