//
//  MskgDownloader.swift
//  rihasapo
//
//  Created by Tetsuya Shiraishi on 2018/01/30.
//  Copyright © 2018年 bianconero, Inc. All rights reserved.
//
// MskgDownloader ver.1.1.0
// 利用されているプロダクト
// rihasapo

import UIKit
import WebKit

class MskgDownloader : NSObject, URLSessionDownloadDelegate{
    
    var iWkWebview:WKWebView? = nil
    var iSaveDirectory:String? = nil
    var iOnComplete:String? = nil
    var iOnProgress:String? = nil
    var iOnError:String? = nil
    var iFiles:[String] = []
    var oneRate:Double = 0
    var dlCounter:Int = 0;
    
    var iTasks:[URLSessionDownloadTask] = []
    var iCancelFlag:Bool = false
    
    static var ins: MskgDownloader = {
        return MskgDownloader()
    }()
    
    private override init(){
        
    }
    
    public func setConfig(
        _wv:WKWebView,
        _saveDir:String,
        _files:[String],
        _onComplete:String,
        _onProgress:String,
        _onError:String
    ){
        self.iWkWebview = _wv
        self.iSaveDirectory = _saveDir
        self.iFiles = _files
        self.iOnComplete = _onComplete
        self.iOnProgress = _onProgress
        self.iOnError = _onError
        
        oneRate = 1
        iCancelFlag = false
    }
    
    public func downloadIt(_id:String, _url:URL){
        
        //let _config = URLSessionConfiguration.background(withIdentifier: "\(Bundle.main.bundleIdentifier!).background.\(_id)")
        let _config = URLSessionConfiguration.background(withIdentifier: "\(Bundle.main.bundleIdentifier!).\(_id)")
        
        // Warning: If an URLSession still exists from a previous download, it doesn't create a new URLSession object but returns the existing one with the old delegate object attached!
        let _session = URLSession(configuration: _config, delegate: self, delegateQueue: OperationQueue())
        
        var _request = URLRequest(url: _url)
        _request.addValue("", forHTTPHeaderField: "Accept-Encoding")
        let _task = _session.downloadTask(with: _request)
//        let _task = _session.downloadTask(with: _url)
        _task.resume()
        
        self.iTasks.append(_task)
        
    }
    
    public func cancelAll(){
        iCancelFlag = true
        for task:URLSessionDownloadTask in self.iTasks {
            print("canceled:",task)
            task.cancel()
        }
    }
    
    
    // MARK: - LocalMethod
    
    // 現在時刻からユニークな文字列を得る
    func getIdFromDateTime() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd-HH-mm-ss"
        return dateFormatter.string(from: Date())
    }
    
    // 保存するディレクトリのパス
    func getSaveDirectory() -> String {
        
        if self.iSaveDirectory != nil {
            return self.iSaveDirectory!
        }else{
            let fileManager = Foundation.FileManager.default
            
            // ライブラリディレクトリのルートパスを取得して、それにフォルダ名を追加
            let path = NSSearchPathForDirectoriesInDomains(Foundation.FileManager.SearchPathDirectory.libraryDirectory, Foundation.FileManager.SearchPathDomainMask.userDomainMask, true)[0] + "/DownloadFiles/"
            
            // ディレクトリがない場合は作る
            if !fileManager.fileExists(atPath: path) {
                createDir(path: path)
            }
            
            return path
        }
        
        
        
        
    }
    
    // ディレクトリを作成
    func createDir(path: String) {
        do {
            let fileManager = Foundation.FileManager.default
            try fileManager.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
        } catch let error as NSError {
            print("createDir: \(error)")
        }
    }
    
    // MARK: - URLSessionDownloadDelegate
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        // ダウンロード完了時の処理
        
        print("didFinishDownloading")
        
        do {
            if let data = NSData(contentsOf: location) {
                
                let fileExtension = location.pathExtension
                var filePath = getSaveDirectory() + getIdFromDateTime() + "." + fileExtension //default
                if let _id = session.configuration.identifier {
                    let filenameID = _id.replacingOccurrences(of: "\(Bundle.main.bundleIdentifier!).", with: "")
                    filePath = getSaveDirectory() + filenameID
                    
                    print("mskg##saved", filenameID)
                    
                    try data.write(toFile: filePath, options: .atomic)
                    
                    //コンプリートイベント
                    let _percentAll = removeMeFromFiles(_filenameID: filenameID)
                    let _script = iOnComplete! + "('\(filenameID)',\(_percentAll));"
                    NotificationCenter.default.post(name: Notification.Name(rawValue:"onCallBackMskgDownloader"), object: nil, userInfo: ["script" : _script])
                }
                
                
            }
        } catch let error as NSError {
            print("download error: \(error)")
        }
    }
    
    
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        // ダウンロード進行中の処理
        
        if iCancelFlag {
            session.invalidateAndCancel()
        }
        
        var progress:Float
        if (totalBytesExpectedToWrite == NSURLSessionTransferSizeUnknown){
            progress = -1
            return //totalBytesExpectedToWriteが不明の場合プログレスはコールしない
        }else{
            progress = Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)
        }
        
        
        // ダウンロードの進捗をログに表示
        //print(String(format: "%.2f", progress * 100) + "%")
        
        // メインスレッドでプログレスバーの更新処理
        DispatchQueue.main.async(execute: {
            //self.progressBar.setProgress(progress, animated: true)
            if let _id = session.configuration.identifier {
                let filenameID = _id.replacingOccurrences(of: "\(Bundle.main.bundleIdentifier!).", with: "")
//                print("mskg##progress", filenameID, String(format: "%.2f", progress * 100) + "%")
                //プログレスイベント
//                let p = String(format: "%.2f", progress * 100)
//                self.iWkWebview!.evaluateJavaScript(self.iOnProgress! + "('\(filenameID)',\(p));", completionHandler: nil)
                let _script = self.iOnProgress! + "('\(filenameID)',\(progress));"
                NotificationCenter.default.post(name: Notification.Name(rawValue:"onCallBackMskgDownloader"), object: nil, userInfo: ["script" : _script])
            }
            
        })
    }
    
    private func removeMeFromFiles(_filenameID:String) -> Double {
        /*
        if self.iFiles.count != 0 {
            oneRate = oneRate + (1.0 / Double(self.iFiles.count))
            print("oneRate", oneRate)
        }else{
            oneRate = 0
        }
        
        return oneRate
        */
        
        var rtn:Double
        if self.iFiles.count != 0 {
            dlCounter = dlCounter + 1;
            rtn = 1.0 - Double(self.iFiles.count - dlCounter) * (1.0 / Double(self.iFiles.count))
        }else{
            rtn = 0
        }
        
        return rtn
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        // ダウンロードエラー発生時の処理
        if error != nil {
            print("download error: \(String(describing: error))")
            
            if let _id = session.configuration.identifier {
                let filenameID = _id.replacingOccurrences(of: "\(Bundle.main.bundleIdentifier!).", with: "")
                //エラーイベント
                //self.iWkWebview!.evaluateJavaScript(iOnError! + "('\(filenameID)');", completionHandler: nil)
                //エラーイベント
                let _script = self.iOnError! + "('\(filenameID)');"
                NotificationCenter.default.post(name: Notification.Name(rawValue:"onCallBackMskgDownloader"), object: nil, userInfo: ["script" : _script])
            }
        }
    }
    
}
