//
//  MskgProdManager.swift
//  rihasapo
//
//  Created by Tetsuya Shiraishi on 2018/02/06.
//  Copyright © 2018年 bianconero, Inc. All rights reserved.
//
// MskgProdManager ver.1.1.0
// 利用されているプロダクト
// rihasapo

import UIKit
import StoreKit
import WebKit



class MskgProdManager: NSObject, SKProductsRequestDelegate, SKPaymentTransactionObserver {
    static var shared = MskgProdManager()
    
    let error_invalidProductIdentifier = "#invalidProductIdentifier"
    let error_failedPaymentQueue = "#paymentQueue:failed"
    
//    var POST_RECEIPT_URL: String {
//        //#if DEBUG
//        //    return "https://sandbox.itunes.apple.com/verifyReceipt"
//        //#else
//        //    return "https://buy.itunes.apple.com/verifyReceipt"
//        //#endif
//        return "https://sandbox.itunes.apple.com/verifyReceipt"
//    }

    
    let POST_RECEIPT_URL = "https://sandbox.itunes.apple.com/verifyReceipt" //https://buy.itunes.apple.com/verifyReceipt
    static var receiptStatus: ReceiptStatusError? = nil
    
    var request:SKProductsRequest!
    @objc dynamic var products:[SKProduct]!
    var iVc:UIViewController?
    
    var iWkWebview:WKWebView? = nil
    var iOnCallback:String? = nil
    var iOnError:String? = nil
    var iProdIds:[String] = []
    var iUserId:String = ""
    var iSecret:String = ""
    
    
    //MARK: コンフィグ設定
    public func setConfig(
        _wv:WKWebView,
        _onCallback:String,
        _onError:String,
        _prodids:[String],
        _userid:String = "",
        _secret:String = ""
        ){
        self.iWkWebview = _wv
        self.iOnCallback = _onCallback
        self.iOnError = _onError
        self.iProdIds = _prodids
        self.iUserId = _userid
        self.iSecret = _secret
    }
    
    
    // 課金可能になっているかどうか
    func canMakePurchases() -> Bool{
        return SKPaymentQueue.canMakePayments()
    }
    
    // Prod情報を取得
    func requestProductsData(vc:UIViewController){
        self.iVc = vc
        self.request = SKProductsRequest(productIdentifiers: Set<String>(self.iProdIds))
        self.request.delegate = self
        self.request.start()
    }
    
    
    // MARK: - 購入メソッド
    func buyProduct(prod:SKProduct){
        let payment = SKMutablePayment(product: prod)
        payment.applicationUsername = self.iUserId //UserManager.sharedInstance.currentUser.token.sha256String()
        SKPaymentQueue.default().add(payment)
    }
    
    // MARK: - リストアメソッド
    func restoreProducts(){
        SKPaymentQueue.default().restoreCompletedTransactions()
    }
    
    // MARK: - SKProductsRequestDelegate
    // リクエストしたProd情報
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        
        self.products = response.products
        
        var invstr = ""
        for id in response.invalidProductIdentifiers{
            print("#invalidProductIdentifier = \(id)")
            invstr = invstr + "#invalid = \(id),"
            
            callBackFunc(_callbackType: self.iOnError!, _arg: self.error_invalidProductIdentifier + "=>\(id)")
        }
        //showMsg(message: invstr)
//        let receiptUrl: NSURL = Bundle.main.appStoreReceiptURL as! NSURL
        
        for prod in self.products {
            //購入手続き開始。配列内に複数のprodidがある場合は、連続して購入ダイアログが出る？
            self.buyProduct(prod: prod)
        }
        
        
    }
    
    
    // MARK: - SKPaymentTransactionObserver
    // app didFinishLaunchingWithOptions で
    // SKPaymentQueue.default().add(MskgProdManager.shared)
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        
        for transaction in transactions {
            switch transaction.transactionState {
            case .purchasing:
                print("#paymentQueue:purchasing")
		//SKPaymentQueue.default().finishTransaction(transaction) //本来は機能解除して得たデータのダウンロードがすべて完了した後にfinishすべき。
            case .deferred:
                print("#paymentQueue:deferred")
            case .failed:
                print("#paymentQueue:failed")
                //queue.finishTransaction(transaction)
                if let errorCallback = self.iOnError{
                    self.callBackFunc(_callbackType: errorCallback, _arg: self.error_failedPaymentQueue)
                }
                SKPaymentQueue.default().finishTransaction(transaction)
            case .purchased:
                print("#paymentQueue:purchased")
                //queue.finishTransaction(transaction)
                print(transaction.payment.applicationUsername) //usernameは購入手続きの精度をあげるのに使うが、今回は特別使用していない
                //completeTransaction(transaction: transaction)
                if let callback = self.iOnCallback{
                    self.callBackFunc(_callbackType: callback, _arg: self.makeBuyProductArg(_transaction: transaction, _type: "purchased"))
                }
                SKPaymentQueue.default().finishTransaction(transaction) //本来は機能解除して得たデータのダウンロードがすべて完了した後にfinishすべき。
            case .restored:
                print("#paymentQueue:restored")
                if let callback = self.iOnCallback{
                    self.callBackFunc(_callbackType: callback, _arg: self.makeBuyProductArg(_transaction: transaction, _type: "restored"))
                }
                SKPaymentQueue.default().finishTransaction(transaction)
            }
        }
        
    }
    
    func makeBuyProductArg(_transaction:SKPaymentTransaction, _type:String) -> String {
        let argObj:[String: String] = [
            "type":_type,
            "prodid":_transaction.payment.productIdentifier,
            "username":_transaction.payment.applicationUsername ?? "---"
        ]
        
        do {
            let jsonBuyProductArgData = try JSONSerialization.data(withJSONObject: argObj, options: [])
            let jsonBuyProductArgStr = String(bytes: jsonBuyProductArgData, encoding: .utf8)!
            
            return jsonBuyProductArgStr
            
        } catch {
            return "error"
        }
    }
    
    
    //MARK: コールバック
    func callBackFunc(_callbackType:String, _arg:String){
        DispatchQueue.main.async(execute: {
            let _script = _callbackType + "('\(_arg)');"
            self.iWkWebview?.evaluateJavaScript(_script, completionHandler: nil)
        })
    }
    
    //MARK: ライセンスフォーマットでコールバック
    func callBackLicense(_callbackType:String, _receipts:[[String: AnyObject]]){
    
        // var licenses:[[String: Bool]] = []
        // for prodid in self.iProdIds {
        //     let o:[String: Bool] = [prodid : checkLicense(_prodid: prodid, _receipts: _receipts)]
        //     licenses.append(o)
        // }
    
        var licenses:[String: Bool] = [:]
        for prodid in self.iProdIds {
            licenses[prodid] = checkLicense(_prodid: prodid, _receipts: _receipts)
        }


//        let licenses = [
//            "osaka.goodlife.rihasapo.individual.basic.OneYear":true,
//            "osaka.goodlife.rihasapo.individual.add02":false,
//            "osaka.goodlife.rihasapo.individual.add07":true
//        ]
        
        //guard let licenses = obj as? [[String: Bool]] else { return }
        
        do {
            let jsonLicensesData = try JSONSerialization.data(withJSONObject: licenses, options: [])
            let jsonLicensesStr = String(bytes: jsonLicensesData, encoding: .utf8)!
            
            self.callBackFunc(_callbackType: self.iOnCallback!, _arg: jsonLicensesStr)
            
        } catch {
            print("error")
        }
        
    }
    
    func checkLicense(_prodid:String, _receipts:[[String: AnyObject]]) -> Bool {
        
        var isLicense = false
        for receipt in _receipts {
            
            if let autoRenewableTypeExpiresDateMS = receipt["expires_date_ms"] as? String {
                //print("receipt * expires_date : ", autoRenewableTypeExpiresDate)

                if (_prodid == receipt["product_id"] as! String){
                    if (UInt64(Date().timeIntervalSince1970 * 1000) < UInt64(autoRenewableTypeExpiresDateMS)!){
                        isLicense = true
                    }
                }
                
            }else{
                //print("receipt * product_id", receipt["product_id"])
                if (_prodid == receipt["product_id"] as! String){
                    return true
                }
                
            }
            
        }
        
        return isLicense
    }
    
    
    
    var receiptStatus:ReceiptStatusError?
    // MARK: - レシート取得メソッド
    //public static func checkReceipt() {
    public func checkReceipt(_licenseFormat:Bool = false) {
        
        do {
            let reqeust = try MskgProdManager.shared.getReceiptRequest() //try getReceiptRequest()
            let session = URLSession.shared
            let task = session.dataTask(with: reqeust, completionHandler: {(data, response, error) -> Void in
                guard let jsonData = data else { return }
                
                do {
                    let json = try JSONSerialization.jsonObject(with: jsonData, options: .init(rawValue: 0)) as AnyObject
                    self.receiptStatus = ReceiptStatusError.statusForErrorCode(json.object(forKey: "status"))
                    guard let latest_receipt_info = (json as AnyObject).object(forKey: "latest_receipt_info") else { return }
                    guard let receipts = latest_receipt_info as? [[String: AnyObject]] else { return }
                    
                    //print(receipts)
                    //updateStatus(receipts: receipts)
                    
                    if(_licenseFormat){ //GetLicenses専用コールバック（ライセンスの有無をフォーマット）
                        self.callBackLicense(_callbackType: self.iOnCallback!,_receipts: receipts)
                    }else{
                        
                        do {
                            let jsonData = try JSONSerialization.data(withJSONObject: receipts, options: [])
                            let jsonStr = String(bytes: jsonData, encoding: .utf8)!
                            //print(jsonStr)  // 生成されたJSON文字列 => {"Name":"Taro"}
                            
                            
                            self.callBackFunc(_callbackType: self.iOnCallback!, _arg: jsonStr)
                            
                            
                        } catch {
                            print("error")
                        }
                    
                    }
                    
                } catch let error {
                    print("SKPaymentManager : Failure to validate receipt: \(error)")
                }
            })
            task.resume()
        } catch let error {
            print("SKPaymentManager : Failure to process payment from Apple store: \(error)")
            
            if let callback = self.iOnCallback {
                if(_licenseFormat){
                    self.callBackLicense(_callbackType: callback,_receipts: [])
                }else{
                    self.callBackFunc(_callbackType: callback, _arg: "[]")
                }
            }
            
            if let errorCallback = self.iOnError {
                self.callBackFunc(_callbackType: errorCallback, _arg: error.localizedDescription)
            }
            
            
            
            
            
            //checkReceiptInLocal()
            //MskgProdManager.shared.checkReceiptInLocal()
        }
    }
    
//    fileprivate static func checkReceiptInLocal() {
    public func checkReceiptInLocal() {
//        let expiresDateMs : UInt64 = 0 //ローカルから取り出してくる
//        let nowDateMs: UInt64 = getNowDateMs()
//
//        if nowDateMs <= expiresDateMs {
//            print("OK")
//        }
    }

    //fileprivate static func getReceiptRequest() throws -> URLRequest {
    public func getReceiptRequest() throws -> URLRequest {
        
        guard let receiptUrl = Bundle.main.appStoreReceiptURL else {
            throw SKError.invalidAppStoreReceiptURL
        }
        
        let receiptData = try Data(contentsOf: receiptUrl)
        let receiptBase64Str = receiptData.base64EncodedString(options: .endLineWithCarriageReturn)
        let requestContents = ["receipt-data": receiptBase64Str, "password": MskgProdManager.shared.iSecret]
        let requestData = try JSONSerialization.data(withJSONObject: requestContents, options: .init(rawValue: 0))
        
        
//        guard let verifyUrl = verifyReceiptUrl else {
//            throw SKError.invalidURL(url: POST_RECEIPT_URL)
//        }
        var request = URLRequest(url: URL(string:MskgProdManager.shared.POST_RECEIPT_URL)!)
        
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField:"content-type")
        request.timeoutInterval = 5.0
        request.httpMethod = "POST"
        request.httpBody = requestData
        
        return request
    }
    
    
}

enum SKError : Error {
    case invalidAppStoreReceiptURL
    case invalidURL(url:String)
}

enum ReceiptStatusError : Error {
    case invalidJson
    case invalidReceiptDataProperty
    case authenticationError
    case commonSecretKeyMisMatch
    case receiptServerError
    case invalidReceiptForProduction
    case invalidReceiptForSandbox
    case unknownError
    
    static func statusForErrorCode(_ _code:Any?) -> ReceiptStatusError? {
        guard let code = _code as? Int else {
            return .unknownError
        }
        switch code {
        case 0:
            return nil
        case 21000:
            return .invalidJson
        case 21002:
            return .invalidReceiptDataProperty
        case 21003:
            return .authenticationError
        case 21004:
            return .commonSecretKeyMisMatch
        case 21005:
            return .receiptServerError
        case 21007:
            return .invalidReceiptForProduction
        case 21008:
            return .invalidReceiptForSandbox
        default:
            return .unknownError
        }
    }
}
