//
//  WkWebFullViewController.swift
//
//  Created by Tetsuya Shiraishi on 2018/01/07.
//  Copyright © 2018年 MUSHIKAGO DESIGN STUDIO CO., LTD. All rights reserved.
//

// version 1.2.0 (Swift3.2) AppCode
// 利用されているプロダクト
// Cava : CoineyKit対応

import UIKit
import WebKit
import AdSupport
import ObjectMapper

// MARK: - 構造体
internal struct WkProp {
    internal var allowFolder:String!
    internal var topIndexFileName:String!
    internal var pinchInZoom:Bool!
    internal var scrollEnabled:Bool!
    internal var bounces:Bool!
    internal var wkBgColor:String!
}

// MARK: - 拡張：色指定
extension UIColor {
    convenience init(hex: String, alpha: CGFloat) {
        if hex.count == 6 {
            let rawValue: Int = Int(hex, radix: 16) ?? 0
            let B255: Int = rawValue % 256
            let G255: Int = ((rawValue - B255) / 256) % 256
            let R255: Int = ((rawValue - B255) / 256 - G255) / 256
            
            self.init(red: CGFloat(R255) / 255, green: CGFloat(G255) / 255, blue: CGFloat(B255) / 255, alpha: alpha)
        } else {
            self.init(red: 0, green: 0, blue: 0, alpha: alpha)
        }
    }
    convenience init(hex: String) {
        self.init(hex: hex, alpha: 1.0)
    }
}

// MARK: Obj for JSON
//downloadFilesToTmp
class DownloadFilesToTmpJSON: Mappable {
    var onProgress: String?
    var onComplete: String?
    var onError: String?
    var serverDirectory: String?
    var downloadFiles:[String]?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        onProgress <- map["onProgress"]
        onComplete <- map["onComplete"]
        onError <- map["onError"]
        serverDirectory <- map["serverDirectory"]
        downloadFiles <- map["downloadFiles"]
    }
}
//License
class LicenseJSON: Mappable {
    var productIds:[String]?
    var onCallBack: String?
    var onError: String?
    var userid:String = ""
    var secret:String = ""
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        productIds <- map["productIds"]
        onCallBack <- map["onCallBack"]
        onError <- map["onError"]
        userid <- map["userid"]
        secret <- map["secret"]
    }
}

//CoineyKitObj
class CoineyJSON: Mappable {
    var cmd:String = ""
    var memo:String = ""
    var amount: Double = 0
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        cmd <- map["cmd"]
        memo <- map["memo"]
        amount <- map["amount"]
    }
}

// MARK: - 本体
class MskgWkWebFullViewController: UIViewController, WKNavigationDelegate, WKScriptMessageHandler {
    
    var wkwv:WKWebView!
    let paths:[AnyObject]! = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true) as [AnyObject]!
    let bundlepath:String = Bundle.main.bundlePath
    
    var iFilePath:String!
    var iAllowFolder:String!
    var iAllowPath:String!
    var iTopFileName:String!
    var iDocumentsPath:String!
    
    var iWkProp:WkProp = WkProp()
    
    // MARK: - JSに対するAPIを定義
    let genericHandlers:[String] = [
        "getUUID",
        "print",
        "copyTmp",
        "getTmpPath",
        "getTmpFileList",
        "downloadFilesToTmp",
        "cancelDownloadTasks",
        "getReceipts",
        "getLisences",
        "buyProduct",
        "restoreProducts",
    ]
    let customHandler:String = "Mskg"
    
    var iDownloadFilesToTmpObj:DownloadFilesToTmpJSON?
    
    //初期化
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        

            //NSArray(contentsOfFile: path)! as [AnyObject]

        
        guard let path:String = Bundle.main.path(forResource: "Mskg", ofType: "plist"),
            let _mskg = NSDictionary(contentsOfFile: path) as? Dictionary<String,AnyObject>,
            let _plist = _mskg["WkProp"]
            else {
            print("mskg.plistに「WkProp」があるか確認してください。")
            return
        }
        
        self.iWkProp.allowFolder = _plist["allowFolder"] as! String
        self.iWkProp.topIndexFileName = _plist["topIndexFileName"] as! String
        self.iWkProp.pinchInZoom = _plist["pinchInZoom"] as! Bool
        self.iWkProp.scrollEnabled = _plist["scrollEnabled"] as! Bool
        self.iWkProp.bounces = _plist["bounces"] as! Bool
        self.iWkProp.wkBgColor = _plist["wkBgColor"] as! String
        
        iAllowFolder = self.iWkProp.allowFolder
        iTopFileName = self.iWkProp.topIndexFileName
        
        
        iDocumentsPath = paths[0] as! String
        //        iAllowPath = iDocumentsPath + "/" + iAllowFolder
        iAllowPath = bundlepath + "/" + iAllowFolder
        iFilePath = iAllowPath + "/" + iTopFileName
        
        // webview内のテキスト選択禁止
        let disableSelectionScriptString = "document.documentElement.style.webkitUserSelect='none';"
        // webview内の長押しによるメニュー表示禁止
        let disableCalloutScriptString = "document.documentElement.style.webkitTouchCallout='none';"
        
        let disableSelectionScript = WKUserScript(source: disableSelectionScriptString, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
        let disableCalloutScript = WKUserScript(source: disableCalloutScriptString, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
        
        let controller = WKUserContentController()
        controller.addUserScript(disableSelectionScript)
        controller.addUserScript(disableCalloutScript)
        
        for handler in genericHandlers {
            controller.add(self, name: handler)
        }
        
        controller.add(self, name: customHandler)
        
        // コンフィグ作成
        let wkcfg = WKWebViewConfiguration()
        wkcfg.userContentController = controller //上記の操作禁止を反映
        wkcfg.ignoresViewportScaleLimits = self.iWkProp.pinchInZoom // ピンチインによるズーム禁止を解除
        wkcfg.websiteDataStore = WKWebsiteDataStore.default() // LocalStorageを許可（黒石追加）
        
        // 上記のコンフィグを反映してWKWebView作成
        wkwv = WKWebView(frame: self.view.frame, configuration: wkcfg)
        
        // iOS11からのドラッグアンドドロップによる画像反転禁止
        if #available(iOS 11.0, *) {
            wkwv.scrollView.subviews.first?.interactions = []
        }
        
        wkwv.scrollView.isScrollEnabled = self.iWkProp.scrollEnabled //スクロール可能に
        wkwv.scrollView.bounces = self.iWkProp.bounces // スクロール時のブラウザのバウンスなし
        
        wkwv.backgroundColor = UIColor(hex:self.iWkProp.wkBgColor)
        
        wkwv.navigationDelegate = self
        
        
        print(iFilePath)
        
        
        wkwv.loadFileURL(URL(fileURLWithPath: iFilePath), allowingReadAccessTo: URL(fileURLWithPath: iAllowPath, isDirectory: true))
        
        self.view.addSubview(wkwv)
        
        // Event
        NotificationCenter.default.addObserver(self, selector: #selector(self.onCallBackMskgDownloader(_:)), name: Notification.Name(rawValue:"onCallBackMskgDownloader"), object: nil)
    }
    
    // ダウンローダーからのイベントの受け止め
    @objc func onCallBackMskgDownloader(_ notification: NSNotification){
        guard
            let _userinfo = notification.userInfo,
            let _script = _userinfo["script"] as? String
            else {
                return
        }
        
        print("####SCRIPT####", _script)
        DispatchQueue.main.async { //MainThreadで実行
            self.wkwv.evaluateJavaScript(_script, completionHandler: nil)
        }
        
    }



    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // WebViewのdelegate
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        let requestURL: URL? = navigationAction.request.url;
        
        // target=blankも開く
        if (navigationAction.navigationType == WKNavigationType.linkActivated) {
            if (navigationAction.targetFrame == nil || navigationAction.targetFrame!.isMainFrame) {
                self.wkwv.load(URLRequest(url: requestURL!))
                decisionHandler(.cancel);
                return;
            }
        }
        
        decisionHandler(.allow)
    }
    
    // 実際のAPIの処理をSwitch文にて記述：WKScriptMessageHandler
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        
        // MARK: - JSから実行されたコマンドの実際の動作
        switch message.name {
                // region //// case print ////

        // MARK: - デバグ用出力コマンド：print
        case "print":
            guard let _arg:String = message.body as? String else {
                print("### print : 引数を文字列に変換できません。")
                break
            }
            print("### print : \(_arg)")
            break
                // endregion
                // region //// case getUUID ////

        // MARK: - 固有ID生成コマンド：getUUID
        case "getUUID":
            guard let _callback:String = message.body as? String else {
                break
            }
//            let script = String(format: "document.getElementById('iPadVolumeData').value = \"%@\";", str)
//            let _uuid = NSUUID().uuidString
            let _uuid = ASIdentifierManager.shared().advertisingIdentifier.uuidString
            let script = _callback + "('\(_uuid)');"
            self.wkwv.evaluateJavaScript(script, completionHandler: nil)
            
            break
                // endregion
                // region //// case copyTmp ////
        // MARK: - www/temp/へファイルをコピー：copyTmp
        case "copyTmp":
            //mskg print("copyTmp 開始")
            
            // バンドル tmp フォルダの一覧を取得
            var tmpFileNames: [String] {
                do {
                    return try FileManager.default.contentsOfDirectory(atPath: bundlepath + "/www/tmp/")
                } catch {
                    return []
                }
            }
            
            // 確認
            print(tmpFileNames)
            
            // ファイル数だけループ
            for tmpFileName in tmpFileNames {
                //print(tmpFileName)
            
                let srcPath = bundlepath + "/www/tmp/" + tmpFileName
                let dstPath = NSTemporaryDirectory() + tmpFileName
                
                // コピー元 /www/tmp/ での存在確認（あって当然だが)
                var srcCheck = "x"
                if( FileManager.default.fileExists( atPath: srcPath ) ) {
                    srcCheck = "o"		//print("srcPath ファイルあり")
                    
                } else {
                    srcCheck = "x"		//print("srcPath ファイルなし")
                }
                
                // コピー先 tmp での存在確認
                var dstCheck = "x"
                if( FileManager.default.fileExists( atPath: dstPath ) ) {
                    dstCheck = "o"		//print("dstPath ファイルあり")
                } else {
                    dstCheck = "x"		//print("dstPath ファイルなし")
                }
                
                // コピー先削除（削除しておかないとコピーは失敗する。存在確認のところでするのが普通だが、まぁこれでもいいよね。今は強制削除＆再コピー。）
                var deleteResult = "-"
                do {
                    try FileManager.default.removeItem(atPath: dstPath)  /* 削除 */
                    deleteResult = "D"
                } catch {
                    deleteResult = "x"		//print("delete 失敗")
                }
                
                
                
                // コピー処理
                var copyResult = "-"
                do {
                    try FileManager.default.copyItem(atPath: srcPath, toPath: dstPath)  /* コピーor移動 */
                    copyResult = "C"
                } catch {
                    copyResult = "x"		//print("copy 失敗")
                }
                
                // 確認
                //mskg print(srcCheck + " " + dstCheck + " " + deleteResult + " " + copyResult + " " + tmpFileName + "    " + dstPath)
                
            }
            
            //mskg print("copyTmp 終了")
            break
                // endregion
                // region //// case print ////
        // MARK: - テンポラリフォルダへパスを取得：getTmpPath
        case "getTmpPath":
            guard let _callback:String = message.body as? String else {
                break
            }
            print("getTmpPath : " + NSTemporaryDirectory())
            let tmpPath = NSTemporaryDirectory()
            let script = _callback + "('\(tmpPath)');"
            self.wkwv.evaluateJavaScript(script, completionHandler: nil)
            //print("getTmpPath : " + NSTemporaryDirectory())
            break
                // endregion
                // region //// case getTmpFileList ////
        // MARK: - テンポラリフォルダのファイルをリスト：getTmpFileList
        case "getTmpFileList":
            guard let _callback:String = message.body as? String else {
                break
            }
            print("getTmpFileList : " + NSTemporaryDirectory())
            let tmpPath = NSTemporaryDirectory()
            
            
            // tmp フォルダの一覧を取得（バンドルではなく実機の本物）
            var tmpFileList: [String] {
                do {
                    return try FileManager.default.contentsOfDirectory(atPath: tmpPath)
                } catch {
                    return []
                }
            }
            print("getTmpFileList : " + tmpFileList.description)
            
            
            let script = _callback + "('\(tmpFileList.description)');"
            self.wkwv.evaluateJavaScript(script, completionHandler: nil)
            //print("getTmpPath : " + NSTemporaryDirectory())
            break
                // endregion
                // region //// case downloadFilesToTmp ////
        // MARK: - テンポラリフォルダのへファイルをダウンロード：downloadFilesToTmp
        case "downloadFilesToTmp":
            
            //messageJSON
            iDownloadFilesToTmpObj = Mapper<DownloadFilesToTmpJSON>().map(JSONString: message.body as! String)
            
    //        iTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.timerUpdate), userInfo: nil, repeats: true)
            
            fetchDownloadFilesToTmp()
            
            //print("downloadFilesToTmp : " + ((message.body as AnyObject) as! String))
            
            break
                // endregion
                // region //// case cancelDownloadTasks ////
        // MARK: - すべてのダウンロードタスクをキャンセル：cancelDownloads
        case "cancelDownloadTasks":
            MskgDownloader.ins.cancelAll()
            break
                // endregion
                // region //// case manageLisence ////
        //アプリ内課金コマンド
        
        // MARK: - アプリ内課金：オリジナルアプリ内課金コマンド：getReceipts
        case "getReceipts": //
            
            let _LicenseJSON = Mapper<LicenseJSON>().map(JSONString: message.body as! String)
            
            guard
                let _onCallback:String = _LicenseJSON?.onCallBack,
                let _onError:String = _LicenseJSON?.onError,
                let _prodids:[String] = _LicenseJSON?.productIds,
                let _secret:String = _LicenseJSON?.secret
                else {
                    return
            }
            
            MskgProdManager.shared.setConfig(
                _wv: self.wkwv,
                _onCallback: _onCallback,
                _onError: _onError,
                _prodids: _prodids,
                _userid: "",
                _secret: _secret
            )
            
            if MskgProdManager.shared.canMakePurchases() {
                MskgProdManager.shared.checkReceipt()
            }else{
                let arg = "アプリ内課金が無効となっています。"
                let _script = _onError + "('\(arg)');"
                self.wkwv?.evaluateJavaScript(_script, completionHandler: nil)
            }
            
            break
                // endregion
        // MARK: - アプリ内課金：オリジナルアプリ内課金コマンド：getLisences
        case "getLisences": //
            
            let _LicenseJSON = Mapper<LicenseJSON>().map(JSONString: message.body as! String)
            
            guard
                let _onCallback:String = _LicenseJSON?.onCallBack,
                let _onError:String = _LicenseJSON?.onError,
                let _prodids:[String] = _LicenseJSON?.productIds,
                let _secret:String = _LicenseJSON?.secret
                else {
                    return
            }
            
            MskgProdManager.shared.setConfig(
                _wv: self.wkwv,
                _onCallback: _onCallback,
                _onError: _onError,
                _prodids: _prodids,
                _userid: "",
                _secret: _secret
            )
            
            if MskgProdManager.shared.canMakePurchases() {
                MskgProdManager.shared.checkReceipt(_licenseFormat: true)
            }else{
                let arg = "アプリ内課金が無効となっています。"
                let _script = _onError + "('\(arg)');"
                self.wkwv?.evaluateJavaScript(_script, completionHandler: nil)
            }
            
            break
            // endregion
        // MARK: - アプリ内課金：オリジナルアプリ内課金コマンド：buyProduct
        case "buyProduct": //
                
                let _LicenseJSON = Mapper<LicenseJSON>().map(JSONString: message.body as! String)
                
                guard
                    let _onCallback:String = _LicenseJSON?.onCallBack,
                    let _onError:String = _LicenseJSON?.onError,
                    let _prodids:[String] = _LicenseJSON?.productIds,
                    let _userid:String = _LicenseJSON?.userid
                    else {
                        return
                }
                
                MskgProdManager.shared.setConfig(
                    _wv: self.wkwv,
                    _onCallback: _onCallback,
                    _onError: _onError,
                    _prodids: _prodids,
                    _userid: _userid,
                    _secret: ""
                )
                
                if MskgProdManager.shared.canMakePurchases() {
                    //                let arg = "アプリ内課金は有効です。"
                    //                let _script = _onCallback + "('\(arg)');"
                    //                self.wkwv?.evaluateJavaScript(_script, completionHandler: nil)
                    
                    MskgProdManager.shared.requestProductsData(vc: self)
                    
                }else{
                    let arg = "アプリ内課金が無効となっています。"
                    let _script = _onError + "('\(arg)');"
                    self.wkwv?.evaluateJavaScript(_script, completionHandler: nil)
                }
                
                break
                // endregion
            
        // MARK: - アプリ内課金：オリジナルアプリ内課金コマンド：restoreProducts
        case "restoreProducts": //
            
            let _LicenseJSON = Mapper<LicenseJSON>().map(JSONString: message.body as! String)
            
            guard
                let _onCallback:String = _LicenseJSON?.onCallBack,
                let _onError:String = _LicenseJSON?.onError,
                let _prodids:[String] = _LicenseJSON?.productIds,
                let _userid:String = _LicenseJSON?.userid
                else {
                    return
            }
            
            MskgProdManager.shared.setConfig(
                _wv: self.wkwv,
                _onCallback: _onCallback,
                _onError: _onError,
                _prodids: _prodids,
                _userid: _userid,
                _secret: ""
            )
            
            if MskgProdManager.shared.canMakePurchases() {
                MskgProdManager.shared.restoreProducts()
            }else{
                let arg = "アプリ内課金が無効となっています。"
                let _script = _onError + "('\(arg)');"
                self.wkwv?.evaluateJavaScript(_script, completionHandler: nil)
            }
            
            break
            // endregion
            
                // region //// case customHandler ////
        case customHandler: //汎用的なカスタムハンドラ
            //MARK: Call MskgCoiney
//            calledFromJS(messageJSON: message.body)
            
            guard
                let _coineyJson = message.body as? String,
                let _coineyObj = Mapper<CoineyJSON>().map(JSONString: _coineyJson)
                else {
                    return
            }
            
            switch _coineyObj.cmd {
                
            case "pay" :
                MskgCoineyKit.ins.iWk = wkwv
                MskgCoineyKit.ins.pay(amount: Int64(floor(_coineyObj.amount)), memo: _coineyObj.memo)
                
                break
                
            case "transaction" :
                print("### called transaction ###");
                
                MskgCoineyKit.ins.openTransactionHistory(container: self)
                
                break
                
            case "coupon" :
                print("### called coupon ###");
                
//                MskgCoineyKit.ins.openTransactionHistory(container: self)
                MskgCoupon.ins.openCoupon(container: self)
                
                break
                
            case "openCouponCamera" :
                print("### called openCouponCamera ###");
                
                //                MskgCoineyKit.ins.openTransactionHistory(container: self)
//                MskgCoupon.ins.openCoupon(container: self)
                
                print("bid=" + _coineyObj.memo)
                
                let qrCameraVC = QRScanViewController()
                qrCameraVC.iBoxID = _coineyObj.memo
                
                // スタイルの指定
                qrCameraVC.modalPresentationStyle = .popover
                // サイズの指定
                qrCameraVC.preferredContentSize = CGSize(width: 300, height: 300)
                // 表示するViewの指定
                qrCameraVC.popoverPresentationController?.sourceView = view
                // ピヨッと表示する位置の指定
                //qrCameraVC.popoverPresentationController?.sourceRect = sender.frame
                // 矢印が出る方向の指定
                qrCameraVC.popoverPresentationController?.permittedArrowDirections = .any
                // デリゲートの設定
                //qrCameraVC.popoverPresentationController?.delegate = self
                //表示
                self.present(qrCameraVC, animated: true, completion: nil)
                
                
                break
                
            default: break
                //cmd default
                print("cmd default")
            }
            
            
            
            break
                // endregion
        default:
            print("message.name = default")
            }
    }
    
    
    //JSからコールされたJSONを確認（デバグ用）
    func calledFromJS(messageJSON:Any){
        print(messageJSON)
    }
    
    var cnt = 0
    var iTimer:Timer = Timer()
    @objc func timerUpdate() {
        cnt += 1
        
        guard
            let _onProgress:String = iDownloadFilesToTmpObj?.onProgress,
            let _onComplete:String = iDownloadFilesToTmpObj?.onComplete
        else {
            return
        }
        //            let script = String(format: "document.getElementById('iPadVolumeData').value = \"%@\";", str)
        //            let _uuid = NSUUID().uuidString
        
        
        if cnt < 100 {
            self.wkwv.evaluateJavaScript(_onProgress + "('117a-02.mp4',\(cnt));", completionHandler: nil)
        }else{
            if iTimer.isValid {
                self.wkwv.evaluateJavaScript(_onProgress + "('117a-02.mp4',\(cnt));", completionHandler: nil)
                self.wkwv.evaluateJavaScript(_onComplete + "('117a-02.mp4');", completionHandler: nil)
                iTimer.invalidate()
            }
        }
        //print("update : " + (iDownloadFilesToTmpObj?.onProgress)! + String(cnt))
    }
    
    // ダウンロードを行う
    func fetchDownloadFilesToTmp(){
        
        guard
        let _obj:DownloadFilesToTmpJSON = iDownloadFilesToTmpObj,
            let _server:String = _obj.serverDirectory,
            let _files:[String] = _obj.downloadFiles
        else {
            return
        }
        
        //DownloadManagerは未使用
//        let _ = DownloadManager.shared.activate()
//        
//        DownloadManager.shared.onProgress = { (progress) in
//            OperationQueue.main.addOperation {
//                print("mskg*", progress)
////                self.progressView.progress = progress
//            }
//        }
        
        
        
        guard
            let _onComplete:String = iDownloadFilesToTmpObj?.onComplete,
            let _onProgress:String = iDownloadFilesToTmpObj?.onProgress,
            let _onError:String = iDownloadFilesToTmpObj?.onError
            else {
                return
        }
        
        MskgDownloader.ins.setConfig(
            _wv: self.wkwv,
            _saveDir: NSTemporaryDirectory(),
            _files: _files,
            _onComplete: _onComplete,
            _onProgress: _onProgress,
            _onError: _onError
        )

        for i in 0..<_files.count {
            
            
            
//            startDownloadTask(_url: _server + _files[i], _filename: _files[i])
            
            
            let filename = _files[i]
            let url = URL(string: _server + filename)
           
            MskgDownloader.ins.downloadIt(_id: filename, _url: url!)
            
//            let task = DownloadManager.shared.activate(id: _files[i]).downloadTask(with: url!)
//            task.resume()
            
        }
        
    }
    
    // 現在時刻からユニークな文字列を得る
    func getIdFromDateTime() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd-HH-mm-ss"
        return dateFormatter.string(from: Date())
    }
    
    // 保存するディレクトリのパス
    func getSaveDirectory() -> String {
        
        let fileManager = Foundation.FileManager.default
        
        // ライブラリディレクトリのルートパスを取得して、それにフォルダ名を追加
        let path = NSSearchPathForDirectoriesInDomains(Foundation.FileManager.SearchPathDirectory.libraryDirectory, Foundation.FileManager.SearchPathDomainMask.userDomainMask, true)[0] + "/DownloadFiles/"
        
        // ディレクトリがない場合は作る
        if !fileManager.fileExists(atPath: path) {
            createDir(path: path)
        }
        
        return path
    }
    
    // ディレクトリを作成
    func createDir(path: String) {
        do {
            let fileManager = Foundation.FileManager.default
            try fileManager.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
        } catch let error as NSError {
            print("createDir: \(error)")
        }
    }
    
//    //回転禁止横向き固定
//    override var shouldAutorotate: Bool {
//        return true
//    }
//
//    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
//        return UIInterfaceOrientationMask.landscape
//    }
//    
//    //////
    
}
