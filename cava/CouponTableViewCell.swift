//
//  CouponTableViewCell.swift
//  cava
//
//  Created by Tetsuya Shiraishi on 2019/03/22.
//  Copyright © 2019年 MP, K.K. All rights reserved.
//

import UIKit
import GTMSessionFetcher
import ObjectMapper

class CouponTableViewCell: UITableViewCell {
    var iAPIServer = "http://tazawa-tori.bianconero.co.jp/dev/tazawa/coupons/setActivity/"
    var iFetcher:GTMSessionFetcher?
    var iCouponID:String?
    var iVc : UIViewController?
    
    @IBOutlet weak var iName: UILabel!
    @IBOutlet weak var iShortName: UILabel!
    @IBOutlet weak var iQRImage: UIImageView!
    @IBOutlet weak var iStartDateTimeButton: UIButton!
    @IBOutlet weak var iEndDateTimeButton: UIButton!
    @IBOutlet weak var iActivitySwitch: UISwitch!
    
    @IBAction func activitySwitchAction(_ sender: UISwitch) {
        
        guard let _couponID = self.iCouponID else {
            return
        }
        
        let _param = (sender.isOn) ? "1" : "0"
        
        let params: [String: String] = [
            "pass": "shoothahazusanai"
        ]
        let paramString = params.stringFromHttpParameters()
        let requestURL = URL(string:"\(iAPIServer)\(_couponID)/\(_param)/?\(paramString)")!
        var request = URLRequest(url: requestURL)
        request.httpMethod = "GET"
        self.iFetcher = GTMSessionFetcher.init(request: request)
        self.iFetcher?.beginFetch(completionHandler: { (data:Data?, error:Error?) in
            if let _data = data{
                if let jsonStr = String(data: _data, encoding: String.Encoding.utf8){
                    if let _ = jsonStr.data(using: .utf8){
                        do {
                            let jsonobj = Mapper<SimpleErrorResults>().map(JSONString: jsonStr)
                            print(jsonobj ?? "SimpleErrorResults return")
                            if (jsonobj?.error == -1){
                                sender.isOn = !(sender.isOn) //元に戻す
                            }
                        }
                    }
                }
                
            }else{
                print("data is empty....")
                
            }
            
            
        })
    }
    
    
    @IBAction func DateTimeStartButtonAction(_ sender:UIButton){
//        MskgCoupon.ins.sample2 { (success:Bool, msg:String) in
//            print([success,msg])
//        }
        
        openDateTimeView(sender: sender, startEnd: "start")
    }
    
    @IBAction func DateTimeEndButtonAction(_ sender:UIButton){
        openDateTimeView(sender: sender, startEnd: "end")
    }
    
    func openDateTimeView(sender:UIButton, startEnd:String = "start"){
        //        let dateTimeVC = DateTimeViewController()
        let dateTimeVC = self.iVc?.storyboard?.instantiateViewController(withIdentifier: "dateTimeViewController") as! DateTimeViewController
        dateTimeVC.iSenderButton = sender
        dateTimeVC.iCouponID = self.iCouponID
        dateTimeVC.iStartEnd = startEnd
        
        if let _dateStr = sender.titleLabel?.text {
            if _dateStr == "未設定" {
                dateTimeVC.iDefaultDateTime = Date()
            }else{
                let dateFormater = DateFormatter()
                dateFormater.locale = Locale(identifier: "ja_JP")
                dateFormater.dateFormat = "yyyy/MM/dd HH:mm:ss"
                dateTimeVC.iDefaultDateTime = dateFormater.date(from: _dateStr)
            }
        }else{
            dateTimeVC.iDefaultDateTime = Date()
        }
        
        
        // スタイルの指定
        dateTimeVC.modalPresentationStyle = .popover
        // サイズの指定
        dateTimeVC.preferredContentSize = CGSize(width: 300, height: 300)
        // 表示するViewの指定
        dateTimeVC.popoverPresentationController?.sourceView = self.iVc?.view
        // ピヨッと表示する位置の指定
        dateTimeVC.popoverPresentationController?.sourceRect = sender.frame
        // 矢印が出る方向の指定
        dateTimeVC.popoverPresentationController?.permittedArrowDirections = .any
        // デリゲートの設定
        //qrCameraVC.popoverPresentationController?.delegate = self
        //表示
        self.iVc?.present(dateTimeVC, animated: true, completion: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
