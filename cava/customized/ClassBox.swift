//
//  ClassBox.swift
//  rihasapo
//
//  Created by Tetsuya Shiraishi on 2018/01/16.
//  Copyright © 2018年 bianconero, Inc. All rights reserved.
//

import UIKit
import ObjectMapper

class ClassBox: NSObject {

}

// 汎用メソッドのためのマップ用オブジェクト

class MskgJSON: Mappable {
    var api: String?
    var callback: String?
    var message: String?
    var num:Double?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        api <- map["api"]
        callback <- map["callback"]
        message <- map["message"]
        num <- map["num"]
    }
}




