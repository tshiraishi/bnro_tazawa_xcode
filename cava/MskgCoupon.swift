//
//  MskgCoupon.swift
//  cava
//
//  Created by Tetsuya Shiraishi on 2019/03/19.
//  Copyright © 2019年 MP, K.K. All rights reserved.
//

import UIKit
import GTMSessionFetcher
import ObjectMapper

class CouponObj: Mappable {
    var id:String?
    var name:String?
    var item_id:String?
    var box_id:String?
    var quantity:String?
    var size:String?
    var coupon_id:String?
    var printout:String?
    var modified:String?
    var created:String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id <- map["Order.id"]
        name <- map["Order.name"]
        item_id <- map["Order.item_id"]
        box_id <- map["Order.box_id"]
        quantity <- map["Order.quantity"]
        size <- map["Order.size"]
        coupon_id <- map["Order.coupon_id"]
        printout <- map["Order.printout"]
        modified <- map["Order.modified"]
        created <- map["Order.created"]
    }
}

class UseCouponResults: Mappable {
    var error:Int?
    var errorMsg:String?
    var result:Array<CouponObj>?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        error <- map["error"]
        errorMsg <- map["errorMsg"]
        result <- map["result"]
    }
}


class MskgCoupon: NSObject {

    static var ins: MskgCoupon = {
        return MskgCoupon()
    }()
    
    private override init(){
        
    }
    
    //MARK: Coupon
    public func openCoupon(container:UIViewController){
        
        let couponVC = container.storyboard!.instantiateViewController(withIdentifier: "couponView")
        
        container.present(couponVC, animated: true, completion: nil)
        
    }
    
    
    public func useCoupon(container:UIViewController, bid:String?, couponCode:String?){
        
        var _fetcher:GTMSessionFetcher?
        var _APIServerUseCoupon = "http://tazawa-tori.bianconero.co.jp/dev/tazawa/orders/useCoupon/"
        
        guard let _bid = bid,
        let _cCode = couponCode
        else {
            return
        }
        
        let params: [String: String] = [
            "pass": "shoothahazusanai"//,
            //            "location": "\(pLat),\(pLng)", //"35.703528,139.560737",
            //            "radius": "\(radius)", //"100",
            //            "type": type ?? "", //"restaurant",
            //            "keyword": keyword ?? ""
        ]
        print(params)
        
        let paramString = params.stringFromHttpParameters()
        let requestURL = URL(string:"\(_APIServerUseCoupon)\(_bid)/\(_cCode)/?\(paramString)")!
        
        var request = URLRequest(url: requestURL)
        request.httpMethod = "GET"
        
        print(request.url)
        
        _fetcher = GTMSessionFetcher.init(request: request)
        
        //        if let _self = self.iSelf{
        //            self.iHud.show(in: _self.view)
        //        }
        
        _fetcher?.beginFetch(completionHandler: { (data:Data?, error:Error?) in
            //self.iHud.dismiss()
            
            
            if let _data = data{
                
                if let jsonStr = String(data: _data, encoding: String.Encoding.utf8){
                    if let _ = jsonStr.data(using: .utf8){
                        
                        do {
                            let jsonobj = Mapper<UseCouponResults>().map(JSONString: jsonStr)
                            print(jsonobj)
                            //NotificationCenter.default.post(name: Notification.Name(rawValue:"onGetPlaces"), object: nil, userInfo: ["results" : jsonobj?.results])
                            
                            //                                print(jsonobj)
                            
                            var message = ""
                            
                            if let _errcode = jsonobj?.error {
                                
                                switch _errcode { //build20 「クーポン」という言葉を「コード情報」に変更
                                case 0:
                                    message = "コード情報が正常に適用されました。"
                                    break
//                                case -2:
//                                    message = "QRコードにコード情報がありません。"
//                                    break
//                                case -3:
//                                    message = "コード情報を認識できませんでした。"
//                                    break
//                                case -4:
//                                    message = "伝票番号に誤りがあります。"
//                                    break
//                                case -5:
//                                    message = "このコード情報は使用済みです。"
//                                    break
                                case -1:
                                    message = "コード情報適用時にエラーが発生しました。"
                                    break
                                default:
                                    message = "コード情報適用時にエラーが発生しました。"
                                    break
                                }
                                
                                
                                if let _errorMsg = jsonobj?.errorMsg {
                                    message = _errorMsg
                                }
                                
                                
                                message = message + "(\(_errcode))"
                            }
                            
                            let alertController = UIAlertController(
                                title: "コード情報識別",
                                message: message,
                                preferredStyle: .alert)
                            let actionOK = UIAlertAction(title: "閉じる", style: .default) {
                                action in
                                // ok code
                                container.dismiss(animated: true, completion: nil)
                            }
                            alertController.addAction(actionOK)
                            
                            container.present(alertController, animated: true, completion: nil)
                            
                            //jsonobj?.entities?.count
//                            if let _result = jsonobj?.result {
//                                self.iTransactionList = _result
//                                self.tableView.reloadData()
//                            }
                            
                            
                            
                        } catch {
                            print(error.localizedDescription)
                            
                        }
                        
                    }
                }
                
            }else{
                print("data is empty....")
                
            }
            
            
        })
        
    }
    
    
    public func sample2(
        callback _block:(Bool, String) -> Void
        )->Void
    {
        _block(true, "--- MskgCoupon sample() is called. ---")
    }
}
