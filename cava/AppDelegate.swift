//
//  AppDelegate.swift
//  cava
//
//  Created by Tetsuya Shiraishi on 2018/08/27.
//  Copyright © 2018年 MP, K.K. All rights reserved.
//

import UIKit
import WebKit
import StoreKit
import ObjectMapper

class SimpleErrorResults: Mappable {
    var error:Int?
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        error <- map["error"]
    }
}

class SimpleMessageResults: Mappable {
    var error:Int?
    var message:String?
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        error <- map["error"]
        message <- map["message"]
    }
}

//extension UIColor {
//    convenience init(hex: String, alpha: CGFloat) {
//        let v = hex.map { String($0) } + Array(repeating: "0", count: max(6 - hex.count, 0))
//        let r = CGFloat(Int(v[0] + v[1], radix: 16) ?? 0) / 255.0
//        let g = CGFloat(Int(v[2] + v[3], radix: 16) ?? 0) / 255.0
//        let b = CGFloat(Int(v[4] + v[5], radix: 16) ?? 0) / 255.0
//        self.init(red: r, green: g, blue: b, alpha: alpha)
//    }
//    
//    convenience init(hex: String) {
//        self.init(hex: hex, alpha: 1.0)
//    }
//}

// https://qiita.com/Simmon/items/80dbf50bc85c527f10f3
extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        WKWebsiteDataStore.default().removeData(ofTypes: [
            WKWebsiteDataTypeDiskCache,
            WKWebsiteDataTypeMemoryCache,
            WKWebsiteDataTypeOfflineWebApplicationCache,
            WKWebsiteDataTypeCookies,
            WKWebsiteDataTypeSessionStorage,
            WKWebsiteDataTypeWebSQLDatabases,
            WKWebsiteDataTypeIndexedDBDatabases
        ], for: [WKWebsiteDataRecord()]) {
            print("キャッシュクリア")
        }
        
        SKPaymentQueue.default().add(MskgProdManager.shared)
        
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

