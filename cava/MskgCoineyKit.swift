//
//  MskgCoineyKit.swift
//  cava
//
//  Created by Tetsuya Shiraishi on 2019/02/07.
//  Copyright © 2019年 MP, K.K. All rights reserved.
//

import UIKit
import WebKit
import CoineyKit
import GTMSessionFetcher
import ObjectMapper


class SentResponseTransactionObj: Mappable { //TransactionObjとは少し違う
    var id:Int?
    var identifier:String?
    var humanReadableIdentifier:String?
    var chargeDate:String?
    var refundDate:String?
    var amount:Int?
    var cardBrand_id:Int?
    var cardSuffix:String?
    var financing_id:Int?
    var refunded:Bool?
    var approvalCode:String?
    var applicationIdentifier:String?
    var applicationLabel:String?
    var cardNetworkDeviceIdentifier:String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id <- map["Transaction.id"]
        identifier <- map["Transaction.identifier"]
        humanReadableIdentifier <- map["Transaction.humanReadableIdentifier"]
        chargeDate <- map["Transaction.chargeDate"]
        refundDate <- map["Transaction.refundDate"]
        amount <- map["Transaction.amount"]
        cardBrand_id <- map["Transaction.cardBrand_id"]
        cardSuffix <- map["Transaction.cardSuffix"]
        financing_id <- map["Transaction.financing_id"]
        refunded <- map["Transaction.refunded"]
        approvalCode <- map["Transaction.approvalCode"]
        applicationIdentifier <- map["Transaction.applicationIdentifier"]
        applicationLabel <- map["Transaction.applicationLabel"]
        cardNetworkDeviceIdentifier <- map["Transaction.cardNetworkDeviceIdentifier"]
        
    }
}

class SentResponseTransactionResults: Mappable {
    var error:Int?
    var result:SentResponseTransactionObj?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        error <- map["error"]
        result <- map["result"]
    }
}

class MskgCoineyKit: NSObject, CYCoineyViewControllerDelegate {

    var iWk:WKWebView?
    var iTopViewController:UIViewController?
    let onCallBack:String = "onMskgCoineyKitCallBack"
    let onErrorCallBack:String = "onMskgCoineyKitErrorCallBack"
    
    var iAPIServer = "http://tazawa-tori.bianconero.co.jp/dev/tazawa/transactions/sendTransaction"
    var iFetcher:GTMSessionFetcher?
    
    var iBoxID:String = "0"
    
    static var ins: MskgCoineyKit = {
        return MskgCoineyKit()
    }()
    
    private override init(){
        
    }
    
    public func pay(amount: Int64, memo: String){
        print([memo, amount])
        
        self.iBoxID = memo ?? "0"
        
        iTopViewController = UIApplication.topViewController()
        if let topController = iTopViewController {
            
            let coineyController = CYCoineyViewController.init(amount: amount, memo: memo, paymentMethod: CYPaymentMethod.creditCard)
            
            coineyController.delegate = self
            
            topController.present(coineyController, animated: true, completion: nil)
        }
        
    }
    
    func coineyViewControllerDidCancel(_ aController: CYCoineyViewController) {
        iTopViewController?.dismiss(animated: true, completion: nil)
        let script = self.onErrorCallBack + "('決済処理をキャンセルしました。');"
        print(script)
        self.iWk?.evaluateJavaScript(script, completionHandler: nil)
    }
    
    func coineyViewController(_ aController: CYCoineyViewController, didComplete aTransaction: CYTransaction) {
        print("Completed transaction: \(String(describing: aTransaction))")
        
        let dateFormat = "yyyy/MM/dd HH:mm:ss"
        
        var jsonDic = Dictionary<String, Any>()
        jsonDic["identifier"] = aTransaction.identifier
        jsonDic["humanReadableIdentifier"] = aTransaction.humanReadableIdentifier
        jsonDic["chargeDate"] = stringFromDate(aTransaction.chargeDate, format: dateFormat)
        
        if let _refundDate = aTransaction.refundDate {
            jsonDic["refundDate"] = stringFromDate(_refundDate, format: dateFormat)
        }
        
        jsonDic["amount"] = aTransaction.amount
        jsonDic["cardBrand_id"] = aTransaction.cardBrand.rawValue
        jsonDic["cardSuffix"] = aTransaction.cardSuffix
        jsonDic["financing_id"] = aTransaction.financing.rawValue
        jsonDic["refunded"] = aTransaction.isRefunded
        jsonDic["approvalCode"] = aTransaction.approvalCode
        jsonDic["applicationIdentifier"] = aTransaction.applicationIdentifier
        jsonDic["applicationLabel"] = aTransaction.applicationLabel
        jsonDic["cardNetworkDeviceIdentifier"] = aTransaction.cardNetworkDeviceIdentifier
        
        var jsonStr = ""
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: jsonDic)
            jsonStr = String(bytes: jsonData, encoding: .utf8) ?? ""
            
            sendTransaction(jsonData: jsonData)
            //print(jsonStr)
        } catch (let e) {
            print(e)
        }
        //player_pay.ctpのonMskgCoineyKitCallBack(arg)にコールバック
        let script = self.onCallBack + "('" + jsonStr + "');"
        print(script)
        self.iWk?.evaluateJavaScript(script, completionHandler: nil)
        
        
        
        iTopViewController?.dismiss(animated: true, completion: nil)
    }
    
    func stringFromDate(_ date: Date, format: String) -> String {
        let formatter: DateFormatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: date)
    }
    
    
    
    
    //MARK: TransactionHistory
    public func openTransactionHistory(container:UIViewController){
        
        let transVC = container.storyboard!.instantiateViewController(withIdentifier: "transactionView") 
        
        container.present(transVC, animated: true, completion: nil)
        
    }
    
    
    func sendTransaction(jsonData:Data){
        
        let params: [String: String] = [
            "pass": "shoothahazusanai",
            //            "location": "\(pLat),\(pLng)", //"35.703528,139.560737",
            //            "radius": "\(radius)", //"100",
            //            "type": type ?? "", //"restaurant",
            //            "keyword": keyword ?? ""
        ]
        print(params)
        
        let paramString = params.stringFromHttpParameters()
        let requestURL = URL(string:"\(iAPIServer)/\(self.iBoxID)/?\(paramString)")!
        
        var request = URLRequest(url: requestURL)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = jsonData
        
        print(request.url)
        
        self.iFetcher = GTMSessionFetcher.init(request: request)
        
        //        if let _self = self.iSelf{
        //            self.iHud.show(in: _self.view)
        //        }
        
        self.iFetcher?.beginFetch(completionHandler: { (data:Data?, error:Error?) in
            //self.iHud.dismiss()
            
            
            if let _data = data{
                
                if let jsonStr = String(data: _data, encoding: String.Encoding.utf8){
                    if let _ = jsonStr.data(using: .utf8){
                        
                        do {
                            let jsonobj = Mapper<SentResponseTransactionResults>().map(JSONString: jsonStr)
                            print(jsonobj ?? "")
                            
                        } catch {
                            print(error.localizedDescription)
                            
                        }
                    }
                }
                
            }else{
                print("data is empty....")
                
            }
            
            
        })
        
        
    }
    
}


