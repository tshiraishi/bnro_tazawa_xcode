//
//  Cfg.swift
//  JiminyCricket
//
//  Created by Tetsuya Shiraishi on 2016/11/14.
//  Copyright © 2016年 MUSHIKAGO DESIGN STUDIO CO., LTD. All rights reserved.
//


open class CfgCava {
    
    class var ins : CfgCava{
        struct Static{
            static let instance : CfgCava = CfgCava()
        }
        return Static.instance
    }
    
    let constTransactionID:String = "tid"
    let constCoupon:String = "cp"
    
    enum ImageSelectMode { //sample
        case nomal
        case pinterest
    }
    
}
