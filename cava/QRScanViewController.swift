//
//  QRScanViewController.swift
//  cava
//
//  Created by Tetsuya Shiraishi on 2019/03/07.
//  Copyright © 2019年 MP, K.K. All rights reserved.
//

import UIKit
import AVFoundation

class QRScanViewController: UIViewController,AVCaptureMetadataOutputObjectsDelegate {

    var session: AVCaptureSession? = nil
    var iPreview:AVCaptureVideoPreviewLayer? = nil
    var iQRCodeFound:Bool = false
    
    var iBoxID:String?
    
    var iTransacrtionTableView:TransactionTableViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // 端末回転の通知機能を設定します。
        let action = #selector(orientationDidChange(_:))
        let center = NotificationCenter.default
        //let name = NSNotification.Name.UIDevice.orientationDidChangeNotification
        let name = UIDevice.orientationDidChangeNotification
        center.addObserver(self, selector: action, name: name, object: nil)
        
        //
        
        
        // Device
        //let device = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        guard let device = AVCaptureDevice.default(for: AVMediaType.video) else { return }
        session = AVCaptureSession.init()
        guard session != nil else {
            print("error")
            return
        }
        
        // Input
        guard let input = try? AVCaptureDeviceInput.init(device: device) else { return }
        session?.addInput(input)
        
        // Output
        let output = AVCaptureMetadataOutput.init()
        session?.addOutput(output)
        output.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        output.metadataObjectTypes = [AVMetadataObject.ObjectType.qr, AVMetadataObject.ObjectType.code128 ]
        
        // Preview
        self.iPreview = AVCaptureVideoPreviewLayer.init(session: session!)
        if let preview = self.iPreview {
            preview.videoGravity = AVLayerVideoGravity.resizeAspectFill;
//            preview.connection.videoOrientation = .landscapeLeft
            rotatePreview()
//            preview.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height);
            preview.frame = CGRect(x: 0, y: 0, width: 300, height: 300);
            self.view.layer.insertSublayer(preview, at: 0)
        }
        
        // Start
        self.session?.startRunning()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //回転
    override func viewDidDisappear(_ animated: Bool) {
        // スーパークラスのメソッドを呼び出します。
        super.viewDidDisappear(animated)
        
        self.iQRCodeFound = false
        
        // 端末回転の通知機能の設定を解除します。
        let center = NotificationCenter.default
        //let name = NSNotification.Name.UIDevice.orientationDidChangeNotification
        let name = UIDevice.orientationDidChangeNotification
        center.removeObserver(self, name: name, object: nil)
    }
    
    @objc func orientationDidChange(_ notification: NSNotification) {
        // 端末の向きを判定します。
        // 縦向きを検知する場合、
        //   device.orientation.isPortrait
        // を判定します。
        
        if UIDevice.current.orientation.isLandscape {
            // 横向きの場合
            rotatePreview()
        }
    }
    
    
    func rotatePreview(){
        let device = UIDevice.current
        if let preview = self.iPreview {
            
            if device.orientation == UIDeviceOrientation.landscapeLeft {
                preview.connection?.videoOrientation = .landscapeRight
            } else if device.orientation == UIDeviceOrientation.landscapeRight {
                preview.connection?.videoOrientation = .landscapeLeft
            }else if(device.orientation == UIDeviceOrientation.faceUp){
                preview.connection?.videoOrientation = .landscapeLeft
            }
        }
    }
    //
    
    
    
//    func captureOutput(_ output: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        
        if iQRCodeFound {
            return
        }
        
        for data in metadataObjects {
            if let code = data as? AVMetadataMachineReadableCodeObject {
                print("type \(code.type)")
                print("result \(code.stringValue)")
                
                if(code.type.rawValue == "org.iso.QRCode"){
                    if let codeStr = code.stringValue{
                        //                        let transactionTableView = self.storyboard?.instantiateViewController(withIdentifier: "transactionTable") as! TransactionTableViewController
                        
                        let codeList = codeStr.components(separatedBy: ":")
                        
                        if (codeList.count != 2){
                            notSupport()
                            return
                        }
                        
                        switch codeList[0]{
                        case CfgCava.ins.constTransactionID: //TransactionIDの認識 tid:
                            let tid = codeList[1]
                            if let transactionTableView = self.iTransacrtionTableView{
                                self.dismiss(animated: true) {
                                    transactionTableView.openTransaction(transactionID: tid)
                                }
                            }
                            break
                        case CfgCava.ins.constCoupon: //クーポンコードの認識 cp:
                            MskgCoupon.ins.useCoupon(container: self, bid: self.iBoxID, couponCode: codeList[1])
                            break
                            
                        default:
                            notSupport()
                        }
                        
                        self.iQRCodeFound = true
                    }
                }
                
            }
        }
    }
    
    
    func notSupport(){
        let alertController = UIAlertController(
            title: "QRコードの認識",
            message: "このQRコードは対応していません。",
            preferredStyle: .alert)
        let actionOK = UIAlertAction(title: "閉じる", style: .default) {
            action in
            // ok code
            self.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(actionOK)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
}

//extension ViewController : AVCaptureMetadataOutputObjectsDelegate {
//    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
//
//        for data in metadataObjects {
//            if let code = data as? AVMetadataMachineReadableCodeObject {
//                print("type \(code.type)")
//                print("result \(code.stringValue)")
//            }
//        }
//    }
//}
