//
//  DateTimeViewController.swift
//  cava
//
//  Created by Tetsuya Shiraishi on 2019/03/23.
//  Copyright © 2019年 MP, K.K. All rights reserved.
//

import UIKit
import GTMSessionFetcher
import ObjectMapper

class DateTimeViewController: UIViewController {
    var iAPIServer = "http://tazawa-tori.bianconero.co.jp/dev/tazawa/coupons/setDateTime/"
    var iFetcher:GTMSessionFetcher?
    
    @IBOutlet weak var datePicker: UIDatePicker!
    var iCouponID:String?
    var iSenderButton:UIButton?
    var iStartEnd:String = "start"
    var iDefaultDateTime:Date?
    
    @IBAction func todayButtonAction(_ sender: UIButton) {
        datePicker.setDate(Date(), animated: true)
    }
    
    @IBAction func setNullButtonAction(_ sender: UIButton) {
        setDateTime(isNull: true)
    }
    @IBAction func setDateTimeButtonAction(_ sender: UIButton) {
        setDateTime(isNull: false)
    }
    
    func setDateTime(isNull:Bool){
        
        guard let _couponID = self.iCouponID else {
            return
        }
        
        let backUpButtonTitle = iSenderButton?.titleLabel?.text
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat  = "yyyy-MM-dd HH:mm:ss";
        let _datetime = (isNull) ? "0" : dateFormatter.string(from: datePicker.date)
        
        let params: [String: String] = [
            "pass": "shoothahazusanai",
            "datetime": _datetime,
            "startend": iStartEnd
        ]
        let paramString = params.stringFromHttpParameters()
        let requestURL = URL(string:"\(iAPIServer)\(_couponID)/?\(paramString)")!
        var request = URLRequest(url: requestURL)
        request.httpMethod = "GET"
        self.iFetcher = GTMSessionFetcher.init(request: request)
        self.iFetcher?.beginFetch(completionHandler: { (data:Data?, error:Error?) in
            if let _data = data{
                if let jsonStr = String(data: _data, encoding: String.Encoding.utf8){
                    if let _ = jsonStr.data(using: .utf8){
                        do {
                            let jsonobj = Mapper<SimpleMessageResults>().map(JSONString: jsonStr)
                            if (jsonobj?.error == -1){
                                self.iSenderButton?.setTitle(backUpButtonTitle, for: UIControl.State.normal)
                            }else{
                                var msg:String? = "未設定"
                                if (jsonobj?.message != nil){
                                    msg = jsonobj?.message
                                }
                                self.iSenderButton?.setTitle(msg, for: UIControl.State.normal)
                            }
                            self.dismiss(animated: true, completion: nil)
                        }
                    }
                }
                
            }else{
                print("data is empty....")
                
            }
            
            
        })
        
        
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        datePicker.date = self.iDefaultDateTime ?? Date()
        let calendar = Calendar(identifier: .gregorian)
        var components = calendar.dateComponents([.year, .month, .day, .hour, .minute, .second, .nanosecond], from: datePicker.date)
        components.second = 0
        components.nanosecond = 0
        let theDate = calendar.date(from: components)!
        datePicker.setDate(theDate, animated: false)
        
        
        // Do any additional setup after loading the view.
//        self.iSenderButton?.setTitle("hoge", for: UIControlState.normal)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
