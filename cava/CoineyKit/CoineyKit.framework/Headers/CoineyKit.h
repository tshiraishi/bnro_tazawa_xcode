// ________________________________________________________________________
// ______///////___________________________________________________________
// ___/////____/////_______________________________________________________
// __///__________///______________________________________________________
// _//______________//______//////_____________//__________________________
// //____//____//___///____//____//____////_________/////____/////__//___//
// //_____//////____///___//__________//__//___//__//___//__//__///__//_//_
// _//______________//_____//____//___//__//___//__//___//__//________///__
// _///____________//________////______////____//__//___//____///______/___
// ___////______////__________________________________________________//___
// ______////////__________________________________________________________
// ________________________________________________________________________

/// \mainpage
/// \~english
/// Copyright © Coiney Inc. All rights reserved.
///
/// For licensing information, contact info\@coiney.com.
/// \~japanese
/// Copyright © Coiney Inc. All rights reserved.
///
/// ライセンス情報については、info\@coiney.com までお問い合わせください。

#import <CoineyKit/CYTypes.h>
#import <CoineyKit/CYCoineyViewController.h>
#import <CoineyKit/CYContainerViewController.h>
#import <CoineyKit/CYAuthenticationViewController.h>
#import <CoineyKit/CYPrinting.h>
#import <CoineyKit/CYTransactionViewController.h>
#import <CoineyKit/CYTransaction.h>
#import <CoineyKit/CYAPIError.h>

FOUNDATION_EXPORT double CoineyKitVersionNumber;
FOUNDATION_EXPORT const unsigned char CoineyKitVersionString[];
