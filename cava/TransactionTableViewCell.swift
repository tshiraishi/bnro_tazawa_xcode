//
//  TransactionTableViewCell.swift
//  cava
//
//  Created by Tetsuya Shiraishi on 2019/03/01.
//  Copyright © 2019年 MP, K.K. All rights reserved.
//

import UIKit

class TransactionTableViewCell: UITableViewCell {

    @IBOutlet weak var iCargeDate: UILabel!
    @IBOutlet weak var iAmount: UILabel!
    @IBOutlet weak var iCardBrandImg: UIImageView!
    @IBOutlet weak var iCardBrand: UILabel!
    @IBOutlet weak var iCardSuffix: UILabel!
    @IBOutlet weak var iFinancing: UILabel!
    @IBOutlet weak var iHumanReadableId: UILabel!
    @IBOutlet weak var iTransactionID: UILabel!
    @IBOutlet weak var iIsRefunded: UIImageView!
    
    @IBOutlet weak var iRefundDate: UILabel!
    
    @IBOutlet weak var iBoxID: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
