//
//  TransactionTableViewController.swift
//  cava
//
//  Created by Tetsuya Shiraishi on 2019/02/25.
//  Copyright © 2019年 MP, K.K. All rights reserved.
//

import UIKit
import CoineyKit
import GTMSessionFetcher
import ObjectMapper

// MARK: - 構造体
//internal struct TransactionObj {
//    internal var id:String!
//    internal var status:Int!
//    internal var title:String!
//}

class TransactionObj: Mappable {
    var id:Int?
    var identifier:String?
    var humanReadableIdentifier:String?
    var chargeDate:String?
    var refundDate:String?
    var amount:String?
    var cardBrand_id:String?
    var cardBrandName:String?
    var cardSuffix:String?
    var financing_id:String?
    var financingName:String?
    var refunded:String?
    var approvalCode:String?
    var applicationIdentifier:String?
    var applicationLabel:String?
    var cardNetworkDeviceIdentifier:String?
    var note:String?
    var box_id:String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id <- map["Transaction.id"]
        identifier <- map["Transaction.identifier"]
        humanReadableIdentifier <- map["Transaction.humanReadableIdentifier"]
        chargeDate <- map["Transaction.chargeDate"]
        refundDate <- map["Transaction.refundDate"]
        amount <- map["Transaction.amount"]
        cardBrand_id <- map["Transaction.cardBrand_id"]
        cardBrandName <- map["CardBrand.name"]
        cardSuffix <- map["Transaction.cardSuffix"]
        financing_id <- map["Transaction.financing_id"]
        financingName <- map["Financing.name"]
        refunded <- map["Transaction.refunded"]
        approvalCode <- map["Transaction.approvalCode"]
        applicationIdentifier <- map["Transaction.applicationIdentifier"]
        applicationLabel <- map["Transaction.applicationLabel"]
        cardNetworkDeviceIdentifier <- map["Transaction.cardNetworkDeviceIdentifier"]
        note <- map["Note.name"]
        box_id <- map["Transaction.box_id"]
        
    }
}

class TransactionResults: Mappable {
    var error:Int?
    var result:Array<TransactionObj>?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        error <- map["error"]
        result <- map["result"]
    }
}


extension String {
    
    /// Percent escapes values to be added to a URL query as specified in RFC 3986
    ///
    /// This percent-escapes all characters besides the alphanumeric character set and "-", ".", "_", and "~".
    ///
    /// http://www.ietf.org/rfc/rfc3986.txt
    ///
    /// :returns: Returns percent-escaped string.
    
    func addingPercentEncodingForURLQueryValue() -> String? {
        let allowedCharacters = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._~")
        
        return self.addingPercentEncoding(withAllowedCharacters: allowedCharacters)
    }
    
}

extension Dictionary {
    
    /// Build string representation of HTTP parameter dictionary of keys and objects
    ///
    /// This percent escapes in compliance with RFC 3986
    ///
    /// http://www.ietf.org/rfc/rfc3986.txt
    ///
    /// :returns: String representation in the form of key1=value1&key2=value2 where the keys and values are percent escaped
    
    func stringFromHttpParameters() -> String {
        let parameterArray = self.map { (key, value) -> String in
            let percentEscapedKey = (key as! String).addingPercentEncodingForURLQueryValue()!
            let percentEscapedValue = (value as! String).addingPercentEncodingForURLQueryValue()!
            return "\(percentEscapedKey)=\(percentEscapedValue)"
        }
        
        return parameterArray.joined(separator: "&")
    }
    
}

class TransactionTableViewController: UITableViewController, UIViewControllerTransitioningDelegate {
    
    var iTransactionList:[TransactionObj] = []
    var iAPIServer = "http://tazawa-tori.bianconero.co.jp/dev/tazawa/transactions/listTransactions"
    var iFetcher:GTMSessionFetcher?
    var iAPIServerRefund = "http://tazawa-tori.bianconero.co.jp/dev/tazawa/transactions/refund"
    var iRefundFetcher:GTMSessionFetcher?
    var iRefresh:UIRefreshControl!
    
    @IBAction func closeBtnAction(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        self.iRefresh = UIRefreshControl()
        self.iRefresh.attributedTitle = NSAttributedString(string: "更新")
        self.iRefresh.addTarget(self, action: "refreshData", for: UIControl.Event.valueChanged)
        self.tableView.addSubview(self.iRefresh)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.refreshData(_:)), name: Notification.Name(rawValue:"refreshTransactionTable"), object: nil)
        
        //いきなりイベント発生させて呼ぶ
        NotificationCenter.default.post(name: Notification.Name(rawValue:"refreshTransactionTable"), object: nil, userInfo: nil)
        //refreshData()
    }
    
    @objc public func refreshData(_ notification: NSNotification){
        let params: [String: String] = [
            "pass": "shoothahazusanai"//,
            //            "location": "\(pLat),\(pLng)", //"35.703528,139.560737",
            //            "radius": "\(radius)", //"100",
            //            "type": type ?? "", //"restaurant",
            //            "keyword": keyword ?? ""
        ]
        print(params)
        
        let paramString = params.stringFromHttpParameters()
        let requestURL = URL(string:"\(iAPIServer)?\(paramString)")!
        
        var request = URLRequest(url: requestURL)
        request.httpMethod = "GET"
        
        print(request.url)
        
        self.iFetcher = GTMSessionFetcher.init(request: request)
        
        //        if let _self = self.iSelf{
        //            self.iHud.show(in: _self.view)
        //        }
        
        self.iFetcher?.beginFetch(completionHandler: { (data:Data?, error:Error?) in
            //self.iHud.dismiss()
            
            
            if let _data = data{
                
                if let jsonStr = String(data: _data, encoding: String.Encoding.utf8){
                    if let _ = jsonStr.data(using: .utf8){
                        
                        do {
                            let jsonobj = Mapper<TransactionResults>().map(JSONString: jsonStr)
                            print(jsonobj)
                            //NotificationCenter.default.post(name: Notification.Name(rawValue:"onGetPlaces"), object: nil, userInfo: ["results" : jsonobj?.results])
                            
                            //                                print(jsonobj)
                            
                            
                            //jsonobj?.entities?.count
                            if let _result = jsonobj?.result {
                                self.iTransactionList = _result
                                self.tableView.reloadData()
                            }
                            
                            
                        } catch {
                            print(error.localizedDescription)
                            
                        }
                        
                        self.iRefresh.endRefreshing()
                    }
                }
                
            }else{
                print("data is empty....")
                
            }
            
            
        })
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return iTransactionList.count
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90.0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as! TransactionTableViewCell

        // Configure the cell...
        let _transaction = iTransactionList[indexPath.row]
        
        cell.iCargeDate.text = _transaction.chargeDate
        
        if let _amount = _transaction.amount {
            var _text = convertMoneyStyle(Value: Int(_amount), Mark: "¥")
            if _transaction.refunded == "1" {
                _text = "(" + _text + ")"
                cell.iAmount.textColor = UIColor.red
            }else{
                cell.iAmount.textColor = UIColor.black
            }
            cell.iAmount.text = _text
        }else{
            cell.iAmount.text = "¥0"
        }
        
        
        cell.iCardSuffix.text = "•••• •••• •••• " + _transaction.cardSuffix!
        cell.iHumanReadableId.text = _transaction.humanReadableIdentifier
        cell.iTransactionID.text = _transaction.identifier
        //cell.iIsRefunded = _transaction.refunded
        
        if let _cardbrand = _transaction.cardBrandName {
            cell.iCardBrand.text = _cardbrand
            cell.iCardBrandImg.image = UIImage(named: "_" + _cardbrand)
        }
        
        var _imageName = "paystatus"
        var _refundDate = ""
        if let _refunded = _transaction.refunded {
            if _refunded == "1"{
                _imageName = "refunded"
                _refundDate = "払い戻し日：" + (_transaction.refundDate ?? "---")
            }else if _refunded == "0"{
                _imageName = "normalpay"
            }
        }
        cell.iIsRefunded.image = UIImage(named: _imageName)
        cell.iRefundDate.text = _refundDate
        
        
        cell.iBoxID.text = "BoxID:" + (_transaction.box_id ?? "0")
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        /*
            CYAuthenticationViewController.checkAuthenticationStatus({ (state:Bool) in
                //CYAuthenticationViewController.deauthenticate()
                if !state {
                    return
                }
                
                if let transactionIdentifier = self.iTransactionList[indexPath.row].identifier {
                    
                    CYLookUpTransaction(transactionIdentifier, { aTransaction, aError in
                        if let transaction = aTransaction {
                            let transCon = CYTransactionViewController.init(transaction: transaction, allowRefunding: true)
                            //                        transactionViewController.navigationItem.rightBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: .done,target: self,action: #selector(self.done))
                            //MARK: トランザクションデリゲート
                            transCon.confirmRefund(completion: { (success:Bool) in
                                print(success)
                                if success {
                                    let params: [String: String] = [
                                        "pass": "shoothahazusanai"
                                    ]
                                    print(params)
                                    
                                    let paramString = params.stringFromHttpParameters()
                                    let requestURL = URL(string:"\(self.iAPIServerRefund)/\(transactionIdentifier)/?\(paramString)")!
                                    
                                    var request = URLRequest(url: requestURL)
                                    request.httpMethod = "GET"
                                    
                                    print(request.url)
                                    
                                    self.iRefundFetcher = GTMSessionFetcher.init(request: request)
                                    
                                    self.iRefundFetcher?.beginFetch(completionHandler: { (data:Data?, error:Error?) in
                                        //self.iHud.dismiss()
                                        
                                        
                                        if let _data = data{
                                            
                                            if let jsonStr = String(data: _data, encoding: String.Encoding.utf8){
                                                if let _ = jsonStr.data(using: .utf8){
                                                    
                                                    do {
                                                        let jsonobj = Mapper<TransactionResults>().map(JSONString: jsonStr)
                                                        print(jsonobj)
                                                        
                                                        if let _error = jsonobj?.error {
                                                            if _error == 0 {
                                                                let alertController = UIAlertController(
                                                                    title: "払い戻し状況",
                                                                    message:  "\(jsonobj?.result?.first?.refundDate ?? "---")に払い戻しを完了。記録をアップデートしました。",
                                                                    preferredStyle: .alert)
                                                                let actionOK = UIAlertAction(title: "OK", style: .default) {
                                                                    action in
                                                                    // ok code
                                                                    NotificationCenter.default.post(name: Notification.Name(rawValue:"refreshTransactionTable"), object: nil, userInfo: nil)
                                                                    self.navigationController?.popViewController(animated: true)
                                                                }
                                                                alertController.addAction(actionOK)
                                                                
                                                                self.present(alertController, animated: true, completion: nil)
                                                            }
                                                        }
                                                        //jsonobj?.entities?.count
//                                                        if let _result = jsonobj?.result {
//                                                            self.iTransactionList = _result
//                                                            self.tableView.reloadData()
//                                                        }
                                                        
                                                        
                                                    } catch {
                                                        print(error.localizedDescription)
                                                        
                                                    }
                                                }
                                            }
                                            
                                        }else{
                                            print("data is empty....")
                                            
                                        }
                                        
                                        
                                    })
                                }
                                
                            })
                            
                            
                            let naviCon = UINavigationController.init(rootViewController: transCon)
                            naviCon.modalPresentationStyle = UIModalPresentationStyle.formSheet
                            
                           self.navigationController?.pushViewController(transCon, animated: true)
                            //self.present(naviCon, animated: true, completion: nil)
                        }
                    })
                    
                }
        }, in: self)
        */
    
        openTransaction(transactionID: self.iTransactionList[indexPath.row].identifier)
        
    }
    
    public func openTransaction(transactionID:String?){
        CYAuthenticationViewController.checkAuthenticationStatus({ (state:Bool) in
            //CYAuthenticationViewController.deauthenticate()
            if !state {
                return
            }
            
            if let transactionIdentifier = transactionID {
                
                CYLookUpTransaction(transactionIdentifier, { aTransaction, aError in
                    if let transaction = aTransaction {
                        let transCon = CYTransactionViewController.init(transaction: transaction, allowRefunding: true)
                        //                        transactionViewController.navigationItem.rightBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: .done,target: self,action: #selector(self.done))
                        //MARK: トランザクションデリゲート
                        transCon.confirmRefund(completion: { (success:Bool) in
                            print(success)
                            if success {
                                let params: [String: String] = [
                                    "pass": "shoothahazusanai"
                                ]
                                print(params)
                                
                                let paramString = params.stringFromHttpParameters()
                                let requestURL = URL(string:"\(self.iAPIServerRefund)/\(transactionIdentifier)/?\(paramString)")!
                                
                                var request = URLRequest(url: requestURL)
                                request.httpMethod = "GET"
                                
                                print(request.url)
                                
                                self.iRefundFetcher = GTMSessionFetcher.init(request: request)
                                
                                self.iRefundFetcher?.beginFetch(completionHandler: { (data:Data?, error:Error?) in
                                    //self.iHud.dismiss()
                                    
                                    
                                    if let _data = data{
                                        
                                        if let jsonStr = String(data: _data, encoding: String.Encoding.utf8){
                                            if let _ = jsonStr.data(using: .utf8){
                                                
                                                do {
                                                    let jsonobj = Mapper<TransactionResults>().map(JSONString: jsonStr)
                                                    print(jsonobj)
                                                    
                                                    if let _error = jsonobj?.error {
                                                        if _error == 0 {
                                                            let alertController = UIAlertController(
                                                                title: "払い戻し状況",
                                                                message:  "\(jsonobj?.result?.first?.refundDate ?? "---")に払い戻しを完了。記録をアップデートしました。",
                                                                preferredStyle: .alert)
                                                            let actionOK = UIAlertAction(title: "OK", style: .default) {
                                                                action in
                                                                // ok code
                                                                NotificationCenter.default.post(name: Notification.Name(rawValue:"refreshTransactionTable"), object: nil, userInfo: nil)
                                                                self.navigationController?.popViewController(animated: true)
                                                            }
                                                            alertController.addAction(actionOK)
                                                            
                                                            self.present(alertController, animated: true, completion: nil)
                                                        }
                                                    }
                                                    //jsonobj?.entities?.count
                                                    //                                                        if let _result = jsonobj?.result {
                                                    //                                                            self.iTransactionList = _result
                                                    //                                                            self.tableView.reloadData()
                                                    //                                                        }
                                                    
                                                    
                                                } catch {
                                                    print(error.localizedDescription)
                                                    
                                                }
                                            }
                                        }
                                        
                                    }else{
                                        print("data is empty....")
                                        
                                    }
                                    
                                    
                                })
                            }
                            
                        })
                        
                        
                        let naviCon = UINavigationController.init(rootViewController: transCon)
                        naviCon.modalPresentationStyle = UIModalPresentationStyle.formSheet
                        
                        self.navigationController?.pushViewController(transCon, animated: true)
                        //self.present(naviCon, animated: true, completion: nil)
                    }
                })
                
            }
        }, in: self)
        
        
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    
    
    //通過表記に変換
    func convertMoneyStyle(Value _val:Int?, Mark _mark:String) -> String{
        if let v = _val{
            var _str:String!
            let _num = NSNumber(value: v)
            let formatter = NumberFormatter()
            formatter.numberStyle = NumberFormatter.Style.decimal
            formatter.groupingSeparator = ","
            formatter.groupingSize = 3
            _str = _mark + formatter.string(from: _num)!
            return _str
        }else{
            return _mark + "0"
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        segue.destination.preferredContentSize = CGSize(width: 300, height: 300)
//        let popView = segue.destination.popoverPresentationController as! QRScanViewController
        let popView = segue.destination as! QRScanViewController
        popView.iTransacrtionTableView = self
    }
}
