//
//  CavaViewController.swift
//  cava
//
//  Created by Tetsuya Shiraishi on 2018/09/13.
//  Copyright © 2018年 MP, K.K. All rights reserved.
//

import UIKit
import WebKit
import ObjectMapper

class CavaViewController: MskgWkWebFullViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func calledFromJS(messageJSON: Any) {
        //messageJSON
        let jsonobj:MskgJSON? = Mapper<MskgJSON>().map(JSONString: messageJSON as! String)
        
        let alertController = UIAlertController(
            title: jsonobj?.message,
            message: "\(jsonobj?.num)",
            preferredStyle: .alert)
        let actionOK = UIAlertAction(title: "OK", style: .default) {
            action in
            // ok code
        }
        alertController.addAction(actionOK)
        
        self.present(alertController, animated: true, completion: nil)
        
        
    }
    
    
}
